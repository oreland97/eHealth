package com.spduniversity.ehealth;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SecurityConfigTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void accessUnprotectedForIndexPage() throws Exception {
        mockMvc.perform(get("/")).andExpect(status().isOk());
    }

    @Test
    public void accessUnprotectedForSearchPage() throws Exception {
        mockMvc.perform(get("/search")).andExpect(status().isOk());
    }

    @Test
    public void accessUnprotectedForClinicInfoPage() throws Exception {
        mockMvc.perform(get("/clinic-info")).andExpect(status().isOk());
    }

    @Test
    public void accessUnprotectedForClinicSignUpPage() throws Exception {
        mockMvc.perform(get("/clinic-sign-up")).andExpect(status().isOk());
    }

    @Test
    public void accessUnprotectedForPatientSignUpPage() throws Exception {
        mockMvc.perform(get("/patient-sign-up")).andExpect(status().isOk());
    }

    @Test
    public void accessProtectedRedirectsToLoginFromPatientEditPage() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/patient-edit"))
                .andExpect(status().is3xxRedirection())
                .andReturn();

        assertThat(mvcResult.getResponse().getRedirectedUrl()).endsWith("/login");
    }

    @Test
    public void accessProtectedRedirectsToLoginFromClinicEditPage() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/clinic-edit"))
                .andExpect(status().is3xxRedirection())
                .andReturn();

        assertThat(mvcResult.getResponse().getRedirectedUrl()).endsWith("/login");
    }

    @Test
    public void accessProtectedRedirectsToLoginFromDoctorsPage() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/doctors-table"))
                .andExpect(status().is3xxRedirection())
                .andReturn();

        assertThat(mvcResult.getResponse().getRedirectedUrl()).endsWith("/login");
    }

    @Test
    public void accessProtectedRedirectsToLoginFromRecordsPage() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/records"))
                .andExpect(status().is3xxRedirection())
                .andReturn();

        assertThat(mvcResult.getResponse().getRedirectedUrl()).endsWith("/login");
    }

    @Test
    public void accessProtectedRedirectsToLoginFromServicesPage() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/services"))
                .andExpect(status().is3xxRedirection())
                .andReturn();

        assertThat(mvcResult.getResponse().getRedirectedUrl()).endsWith("/login");
    }

    @Test
    public void accessProtectedRedirectsToLoginFromBookingPage() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/booking"))
                .andExpect(status().is3xxRedirection())
                .andReturn();

        assertThat(mvcResult.getResponse().getRedirectedUrl()).endsWith("/login");
    }

    @Test
    public void loginPatient() throws Exception {
        this.mockMvc.perform(formLogin("/login")
                .user("mike@gmail.com")
                .password("123"))
                .andExpect(authenticated());
    }

    @Test
    public void loginClinic() throws Exception {
        this.mockMvc.perform(formLogin("/login")
                .user("clinic@gmail.com")
                .password("123"))
                .andExpect(authenticated());
    }


    @Test
    public void loginInvalidUser() throws Exception {
        this.mockMvc.perform(formLogin().user("invalid").password("invalid"))
                .andExpect(unauthenticated())
                .andExpect(status().is3xxRedirection());
    }

    @Test
    public void loginPatientAccessProtected() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(formLogin().user("mike@gmail.com").password("123"))
                .andExpect(authenticated()).andReturn();

        MockHttpSession httpSession = (MockHttpSession) mvcResult.getRequest().getSession(false);

        this.mockMvc.perform(get("/patient-edit").session(httpSession))
                .andExpect(status().isOk());
    }

    @Test
    public void loginClinicAccessProtected() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(formLogin().user("clinic@gmail.com").password("123"))
                .andExpect(authenticated()).andReturn();

        MockHttpSession httpSession = (MockHttpSession) mvcResult.getRequest().getSession(false);

        this.mockMvc.perform(get("/clinic-edit").session(httpSession))
                .andExpect(status().isOk());
    }

}
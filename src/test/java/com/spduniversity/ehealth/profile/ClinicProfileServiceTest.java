package com.spduniversity.ehealth.profile;

import com.spduniversity.ehealth.model.Clinic;
import com.spduniversity.ehealth.profile.services.ClinicProfileService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class ClinicProfileServiceTest {

    private static ClinicProfileService clinicProfileService;

//    @BeforeAll
//    static void initializeClinicProfileService() {
//        clinicProfileService = new ClinicProfileService();
//    }

    @Test
    void shouldReturnClinicByEmail() {
        String actualClinicName = clinicProfileService
                .getClinicByEmail("clinic_test@gmail.com")
                .getName();
        String expectedClinicName = "Test Clinic";
        assertEquals(actualClinicName, expectedClinicName);
    }

    @Test
    void shouldReturnNullWhenInvalidEmail() {
        Clinic actualClinic = clinicProfileService.getClinicByEmail("fail@gmail.com");
        assertNull(actualClinic);
    }

}
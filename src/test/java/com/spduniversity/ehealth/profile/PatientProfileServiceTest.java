package com.spduniversity.ehealth.profile;

import com.spduniversity.ehealth.model.Patient;
import com.spduniversity.ehealth.profile.services.PatientProfileService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class PatientProfileServiceTest {

    private static PatientProfileService patientProfileService;

//    @BeforeAll
//    static void initializePatientProfileService() {
//        patientProfileService = new PatientProfileService();
//    }

    @Test
    void shouldReturnPatientByEmail() {
        String actualPatientName = patientProfileService
                .getPatientByEmail("mike@gmail.com")
                .getFirstName();
        String expectedPatientName = "Mike";
        assertEquals(actualPatientName, expectedPatientName);
    }

    @Test
    void shouldReturnNullWhenInvalidEmail() {
        Patient actualPatient = patientProfileService.getPatientByEmail("men@gmail.com");
        assertNull(actualPatient);
    }

}
$(document).ready(function () {
    var clinicFromResponse;
    var selectedCity;
    var cityFromResponse;
    var specializationsFromResponse;

    $('#clinic_spec').select2({});

    $.ajax({
        url: 'api/clinic/profile/specializations',
        type: 'GET',
        success: function (response) {
            specializationsFromResponse = response;
            var defOptionForSpec = new Option("Choose specializations...");
            $(defOptionForSpec).val('').html("Choose specializations...");
            $('#clinic_spec').append(defOptionForSpec);
            var option;
            var selectedSpec = document.getElementById("clinic_spec").value;
            for (var i = 0; i < response.length; i++) {
                if (selectedSpec === response[i].id) {
                    option = new Option(response[i].name, response[i].id, true, true);
                    $(option).text((response[i].name));
                    $(option).val((response[i].id));
                    $('#clinic_spec').append(option);
                }
                else {
                    option = new Option((response));
                    $(option).text((response[i].name));
                    $(option).val((response[i].id));
                    $('#clinic_spec').append(option);
                }
            }

        }
    });

    $.ajax({
        url: 'api/clinic/profile',
        type: 'GET',
        success: function (data) {

            clinicFromResponse = data;
            $('#input_clinic_name').val(data.name);
            $('#input_clinic_email').val(data.email);
            $('#input_clinic_url').val(data.url);
            $('#input_clinic_phone').val(data.phone);
            $('#input_clinic_description').val(data.description);
            $('#clinic_time_of_start').val(data.schedule.timeOfStart);
            $('#clinic_time_of_end').val(data.schedule.timeOfEnd);
            setSpecializationsToClinic(data.specializations);
            setRegionAndLocationFromResponse(data);
            cityFromResponse = data.address.city;
            setCityFromResponse(cityFromResponse);
        }
    });

    $('#input_clinic_email').on('keyup', function () {
        var email = $('#input_clinic_email').val();
        if (isEmail(email) && email.length < 30) {
            $('#save_clinic').prop('disabled', false);
            $('#clinic_reg_email_message').html('');
        }
        else {
            $('#clinic_reg_email_message').html('Зайві символи в email!').css('color', 'red');
            $('#save_clinic').prop('disabled', true);
        }
    });

    $('#input_clinic_name').on('keyup', function () {
        var name = $('#input_clinic_name').val();
        if (isValidNames(name) && name.length < 50) {
            $('#save_clinic').prop('disabled', false);
            $('#name_edit_Message').html('');
        }
        else {
            $('#name_edit_Message').html('Зайві символи в імені!').css('color', 'red');
            $('#save_clinic').prop('disabled', true);
        }
    });

    $('#input_clinic_url').on('keyup', function () {
        var url = $('#input_clinic_url').val();
        if (isValidUrl(url) && url.length < 50) {
            $('#save_clinic').prop('disabled', false);
            $('#clinic_url_message').html('');
        }
        else {
            $('#clinic_url_message').html('URL має бути формату: https://www.google.com').css('color', 'red');
            $('#save_clinic').prop('disabled', true);
        }
    });

    $('#input_clinic_phone').on('keyup', function () {
        var phone = $('#input_clinic_phone').val();
        if (isValidNumber(phone) && phone.length < 10) {
            $('#save_clinic').prop('disabled', false);
            $('#clinic_phone_message').html('');
        }
        else {
            $('#clinic_phone_message').html('Зайві символи в номері телефону!').css('color', 'red');
            $('#save_clinic').prop('disabled', true);
        }
    });

    $('#location').on('keyup', function () {
        var location = $('#location').val();
        if (isValidLocation(location) && location.length < 50) {
            $('#save_clinic').prop('disabled', false);
            $('#clinic_location_message').html('');
        }
        else {
            $('#clinic_location_message').html('Зайві символи в адресі!').css('color', 'red');
            $('#save_clinic').prop('disabled', true);
        }
    });

    $('#input_clinic_description').on('keyup', function () {
        var descr = $('#input_clinic_description').val();
        if (isValidNames(descr) && descr.length < 200) {
            $('#save_clinic').prop('disabled', false);
            $('#clinic_descr_message').html('');
        }
        else {
            $('#clinic_descr_message').html('Зайві символи в описі!').css('color', 'red');
            $('#save_clinic').prop('disabled', true);
        }
    });


    $.ajax({
        url: 'api/localization/regions',
        type: 'GET',
        success: function (response) {
            $('#region').empty();
            var defOptionForRegion = new Option("Оберіть область...");
            $(defOptionForRegion).val('').html("Оберіть область...");
            $('#region').append(defOptionForRegion);
            var option;
            var selectedRegion = document.getElementById("region").value;
            for (var i = 0; i < response.length; i++) {
                if (selectedRegion === response[i]) {
                    option = new Option((response)[i], (response)[i], true, true);
                    $(option).html((response)[i]);
                    $('#region').append(option);
                }
                else {
                    option = new Option((response)[i]);
                    $(option).html((response)[i]);
                    $('#region').append(option);
                }
            }
        }
    });

    $('#region').on('change', function () {
        var selectedRegion = $('#region').val();
        $.ajax({
            url: 'api/localization/cities',
            type: 'GET',
            data: {
                "selectedRegion": selectedRegion
            },
            success: function (response) {
                $('#city').empty();
                var defOptionForCity = new Option("Оберіть місто...");
                $(defOptionForCity).val('').html("Оберіь місто...");
                $('#city').append(defOptionForCity);
                var option;
                selectedCity = document.getElementById("city").value;
                for (var i = 0; i < response.length; i++) {
                    if (selectedCity === response[i]) {
                        option = new Option((response)[i], (response)[i], true, true);
                        $(option).html((response)[i]);
                        $('#city').append(option);
                    }
                    else {
                        option = new Option((response)[i]);
                        $(option).html((response)[i]);
                        $('#city').append(option);
                    }
                }
            }
        });
    });

    $('#save_clinic').click(function () {
        var addressObj = {
            "region": $('#region').val(),
            "city": $('#city').val(),
            "location": $('#location').val()
        };

        clinicFromResponse.name = $('#input_clinic_name').val();
        clinicFromResponse.email = $('#input_clinic_email').val();
        clinicFromResponse.url = $('#input_clinic_url').val();
        clinicFromResponse.phone = $('#input_clinic_phone').val();
        clinicFromResponse.schedule.timeOfStart = $('#clinic_time_of_start').val();
        clinicFromResponse.schedule.timeOfEnd = $('#clinic_time_of_end').val();
        clinicFromResponse.specializations = $("#clinic_spec").select2().val();
        clinicFromResponse.description = $('#input_clinic_description').val();
        clinicFromResponse.address = addressObj;

        if (clinicFromResponse.name !== '' && clinicFromResponse.email !== '' &&
            clinicFromResponse.specializations !== '' && addressObj.region !== '' &&
            addressObj.city !== '' && addressObj.location !== '') {
            $.ajax({
                url: 'api/clinic/profile',
                type: 'POST',
                dataType: 'json',
                contentType: "application/json",
                data: JSON.stringify(clinicFromResponse),
                statusCode: {
                    200: function (data) {
                        window.location.replace("/search");
                    }
                }
            });
            return false;
        }

    });

    $('#clinic_cancel').click(function () {
        window.location.replace("/search");
    });

    function setSpecializationsToClinic(specializations) {
        $.each(specializations, function (i, item) {
            var optionSpec = $('#clinic_spec option').filter(function () {
                return $(this).html() == item;
            });
            var $newOption = $(optionSpec);
            $("#clinic_spec").append($newOption.attr('selected', 'selected')).trigger('change');
        });
    }

    function setCityFromResponse(city) {
        if (city != null) {
            $.ajax({
                url: 'api/localization/cities',
                type: 'GET',
                data: {
                    "selectedRegion": $('#region').val()
                },
                success: function (response) {
                    $('#city').empty();

                    $.each(response, function (i, item) {
                        if (item === city) {
                            $('#city').append($('<option selected="selected" ' +
                                'value="' + response[i] + '">' + response[i] + '</option>'
                            ));
                        }
                        else {
                            $('#city').append($('<option>', {
                                value: item,
                                text: item
                            }));
                        }
                    });
                }

            });

        }
    }

});

function setRegionAndLocationFromResponse(data) {
    var region = data.address.region;
    var location = data.address.location;
    if (region !== null) {
        $('#region').val(region);
    }
    if (location !== null) {
        $('#location').val(location);
    }
}


function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function isValidNames(name) {
    var rv_name = /^[А-Яа-яЁёЇїІіЄєҐґ'.,"\s\d-]+$/;
    return rv_name.test(name);
}

function isValidUrl(url) {
    var webRegex = /^(ftp|http|https):\/\/[^ "]+$/;
    return webRegex.test(url);
}

function isValidNumber(num) {
    var numRegex = /^\d+$/;
    return numRegex.test(num);
}

function isValidLocation(location) {
    var locationRegex = /^[А-Яа-яЁёЇїІіЄєҐґ,.'\s\d-]*$/;
    return locationRegex.test(location);
}
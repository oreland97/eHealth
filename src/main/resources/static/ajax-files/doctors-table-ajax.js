$(document).ready(function () {
    var doctorIdForEdit;
    var schedules = [];

    $.ajax({
        url: 'api/clinic/profile/doctors',
        type: 'GET',
        success: function (response) {
            $(function () {
                $.each(response, function (i, item) {
                    var trHTML = '<tr>' +
                        '<td>' + item.firstName + '</td>' +
                        '<td>' + item.secondName + '</td>' +
                        '<td>' + item.specialization + '</td>' +
                        '<td>' + item.phone + '</td>' +
                        '<td><p data-placement="top">\n' +
                        '                <button class="btn btn-primary" value="' + item.id + '" id="doc_schedule_btn" data-toggle="modal" data-title="Schedule"\n' +
                        '                        data-target="#schedule_doctor_modal">\n' +
                        '                    <span class="fa fa-calendar"></span>\n' +
                        '                </button>\n' +
                        '            </p></td>' +
                        '<td id="doc_edit">\n' +
                        '            <p data-placement="top">\n' +
                        '                <button class="btn btn-primary" value="' + item.id + '" type="button" id="doc_edit_btn" data-toggle="modal"\n' +
                        '                        data-target="#edit_doctor_modal">\n' +
                        '                    <span class="fa fa-pencil-square-o"></span>\n' +
                        '                </button>\n' +
                        '            </p>\n' +
                        '        </td>' +
                        '<td><p data-placement="top" data-toggle="tooltip" title="Delete">\n' +
                        '                <button class="btn btn-danger" value="' + item.id + '" id="doc_delete_btn" data-toggle="modal" data-title="Delete"\n' +
                        '                        data-target="#delete_doctor_modal">\n' +
                        '                    <span class="fa fa-times"></span>\n' +
                        '                </button>\n' +
                        '            </p></td>' +
                        '</tr>';
                    $('#doctors_table').append(trHTML);
                });
            });
        }
    });


    $.ajax({
        url: 'api/clinic/profile/specializations',
        type: 'GET',
        success: function (response) {
            var defOptionForSpec = new Option("Оберіть спеціалізацію...");
            $(defOptionForSpec).html("Оберіть спеціалізацію...");
            $('#edit_doc_specialization').append(defOptionForSpec);
            var option;
            var selectedSpec = document.getElementById("edit_doc_specialization").value;
            for (var i = 0; i < response.length; i++) {
                if (selectedSpec === response[i].id) {
                    option = new Option(response[i].name, response[i].id, true, true);
                    $(option).text((response[i].name));
                    $(option).val((response[i].id));
                    $('#edit_doc_specialization').append(option);
                }
                else {
                    option = new Option((response));
                    $(option).text((response[i].name));
                    $(option).val((response[i].id));
                    $('#edit_doc_specialization').append(option);
                }
            }

        }
    });

    $('#edit_doctor_modal').on('show.bs.modal', function (e) {
        var button = e.relatedTarget;
        var value = button.value;

        $.ajax({
            url: 'api/clinic/profile/get-doctor',
            type: 'GET',
            dataType: 'json',
            contentType: "application/json",
            data: {
                "id": value
            },
            success: function (response) {
                doctorIdForEdit = value;
                $('#edit_doc_first_name').val(response.firstName);
                $('#edit_doc_second_name').val(response.secondName);

                var optionSpec = $('#edit_doc_specialization option')
                    .filter(function () {
                        return $(this).html() == response.specialization;
                    });
                var $newOption = $(optionSpec);
                $("#edit_doc_specialization").append($newOption.attr('selected', 'selected')).trigger('change');

                $('#edit_doc_phone').val(response.phone);
                $('#edit_doc_description').val(response.description);

            }
        });

        $('#edit_doc_first_name').on('keyup', function () {
            var fName = $('#edit_doc_first_name').val();
            if (isValidNames(fName) && fName.length < 20){
                $('#doc_save').prop('disabled', false);
                $('#fName_editDoc_Message').html('');
            }
            else {
                $('#fName_editDoc_Message').html('Зайві символи в імені!').css('color', 'red');
                $('#doc_save').prop('disabled', true);
            }
        });

        $('#edit_doc_second_name').on('keyup', function () {
            var sName = $('#edit_doc_second_name').val();
            if (isValidNames(sName) && sName.length < 20){
                $('#doc_save').prop('disabled', false);
                $('#sName_editDoc_Message').html('');
            }
            else {
                $('#sName_editDoc_Message').html('Зайві символи в прізвищі!').css('color', 'red');
                $('#doc_save').prop('disabled', true);
            }
        });

        $('#edit_doc_phone').on('keyup', function () {
            var phone = $('#edit_doc_phone').val();
            if (isValidNumber(phone) && phone.length < 10){
                $('#doc_save').prop('disabled', false);
                $('#phone_editDoc_Message').html('');
            }
            else {
                $('#phone_editDoc_Message').html('Зайві символи в номері телефону!').css('color', 'red');
                $('#doc_save').prop('disabled', true);
            }
        });

        $('#edit_doc_description').on('keyup', function () {
            var descr = $('#edit_doc_description').val();
            if (isValidNames(descr) && descr.length < 200) {
                $('#doc_save').prop('disabled', false);
                $('#descr_editDoc_Message').html('');
            }
            else {
                $('#descr_editDoc_Message').html('Зайві символи в описі!').css('color', 'red');
                $('#doc_save').prop('disabled', true);
            }
        });


        $('#edited_doc_save').click(function () {
            var doctorObj = {
                "id": doctorIdForEdit,
                "firstName": $('#edit_doc_first_name').val(),
                "secondName": $('#edit_doc_second_name').val(),
                "specialization": $('#edit_doc_specialization').val(),
                "phone": $('#edit_doc_phone').val(),
                "description": $('#edit_doc_description').val()
            };
            if (doctorObj.firstName !== '' && doctorObj.secondName !== '' &&
                doctorObj.specialization !== '') {
                $.ajax({
                    url: "api/clinic/profile/update-doctor",
                    type: 'POST',
                    dataType: 'json',
                    contentType: "application/json",
                    data: JSON.stringify(doctorObj),
                    statusCode: {
                        200: function (data) {
                            window.location.replace("doctors-table");
                        }
                    }
                });
                return false;
            }

        });

        $('#edit_doctor_modal').on('hide.bs.modal', function (e) {
            location.reload();
        })
    });

    $('#schedule_doctor_modal').on('show.bs.modal', function (e) {
        $('.form-control').val('');
        var button = e.relatedTarget;
        var value = button.value;


        $.ajax({
            url: 'api/clinic/profile/get-doctor',
            type: 'GET',
            dataType: 'json',
            contentType: "application/json",
            data: {
                "id": value
            }, success: function (response) {
                var schedules = response.schedules;
                for (var i = 0; i < schedules.length; i++) {
                    if (schedules[i] !== null) {
                        $('#' + schedules[i].dayOfWeek + '_Start').val(schedules[i].timeOfStart);
                        $('#' + schedules[i].dayOfWeek + '_End').val(schedules[i].timeOfEnd);
                    }
                }
            }
        });

        $('#schedule_doc_save').click(function () {

            var dayOfWeeks = [
                'MONDAY', 'TUESDAY',
                'WEDNESDAY', 'THURSDAY', 'FRIDAY'
            ];

            schedules = [];
            var oneSchedule;

            for (var i = 0; i < dayOfWeeks.length; i++) {
                oneSchedule = {
                    "dayOfWeek" : dayOfWeeks[i],
                    "timeOfStart" : $('#' + dayOfWeeks[i] + '_Start').val(),
                    "timeOfEnd" : $('#' + dayOfWeeks[i] + '_End').val()
                };
                schedules.push(oneSchedule);
            }

            var doctors = {
                'id' : value,
                'schedules' : schedules
            };

            $.ajax({
                url: 'api/clinic/profile/update-doctor-schedule',
                type: 'POST',
                dataType: 'json',
                contentType: "application/json",
                data: JSON.stringify(doctors),
                statusCode: {
                    200: function (data) {
                        location.reload();
                    }
                }
            });

        });
        $('#schedule_doctor_modal').on('hide.bs.modal', function (e) {
            location.reload();
        })
    });

    $('#add_doctor').click(function () {
        $.ajax({
            url: 'api/clinic/profile/specializations',
            type: 'GET',
            success: function (response) {
                var defOptionForSpec = new Option("Оберіть спеціалізацію...");
                $(defOptionForSpec).html("Оберіть спеціалізацію...");
                $('#doc_specialization').append(defOptionForSpec);
                var option;
                var selectedSpec = document.getElementById("doc_specialization").value;
                for (var i = 0; i < response.length; i++) {
                    if (selectedSpec === response[i].id) {
                        option = new Option(response[i].name, response[i].id, true, true);
                        $(option).text((response[i].name));
                        $(option).val((response[i].id));
                        $('#doc_specialization').append(option);
                    }
                    else {
                        option = new Option((response));
                        $(option).text((response[i].name));
                        $(option).val((response[i].id));
                        $('#doc_specialization').append(option);
                    }
                }

            }
        });

        $('#doc_first_name').on('keyup', function () {
            var fName = $('#doc_first_name').val();
            if (isValidNames(fName) && fName.length < 20){
                $('#doc_save').prop('disabled', false);
                $('#fName_newDoc_Message').html('');
            }
            else {
                $('#fName_newDoc_Message').html('Зайві символи в імені!').css('color', 'red');
                $('#doc_save').prop('disabled', true);
            }
        });

        $('#doc_second_name').on('keyup', function () {
            var sName = $('#doc_second_name').val();
            if (isValidNames(sName) && sName.length < 20){
                $('#doc_save').prop('disabled', false);
                $('#sName_newDoc_Message').html('');
            }
            else {
                $('#sName_newDoc_Message').html('Зайві символи в прізвищі!').css('color', 'red');
                $('#doc_save').prop('disabled', true);
            }
        });

        $('#doc_phone').on('keyup', function () {
            var phone = $('#doc_phone').val();
            if (isValidNumber(phone) && phone.length < 10){
                $('#doc_save').prop('disabled', false);
                $('#phone_newDoc_Message').html('');
            }
            else {
                $('#phone_newDoc_Message').html('Зайві символи в номері телефону!').css('color', 'red');
                $('#doc_save').prop('disabled', true);
            }
        });

        $('#doc_description').on('keyup', function () {
            var descr = $('#doc_description').val();
            if (isValidNames(descr) && descr.length < 200) {
                $('#doc_save').prop('disabled', false);
                $('#descr_newDoc_Message').html('');
            }
            else {
                $('#descr_newDoc_Message').html('Зайві символи в описі!').css('color', 'red');
                $('#doc_save').prop('disabled', true);
            }
        });

        $('#doc_save').click(function () {
            var doctorObj = {
                "firstName": $('#doc_first_name').val(),
                "secondName": $('#doc_second_name').val(),
                "specialization": $('#doc_specialization').val(),
                "phone": $('#doc_phone').val(),
                "description": $('#doc_description').val()
            };

            if (doctorObj.firstName !== '' && doctorObj.secondName !== '' &&
                doctorObj.specialization !== '') {
                $.ajax({
                    url: 'api/clinic/profile/add-doctor',
                    type: 'POST',
                    dataType: 'json',
                    contentType: "application/json",
                    data: JSON.stringify(doctorObj),
                    statusCode: {
                        200: function (data) {
                            window.location.replace("doctors-table");
                        }
                    }
                });
                return false;
            }

        });
    });

    $('#delete_doctor_modal').on('show.bs.modal', function (e) {
        var button = e.relatedTarget;
        var value = button.value;

        $('#delete_doc_btn').click(function () {
            $.ajax({
                url: 'api/clinic/profile/delete-doctor' + '?id=' + value,
                type: 'DELETE',
                dataType: 'json',
                contentType: "application/json",
                statusCode: {
                    200: function (data) {
                        location.reload();
                    }
                }
            });
        });
    });
});

function isValidNames(name) {
    var rv_name = /^[А-Яа-яЁёЇїІіЄєҐґ'.,"\s\d-]+$/;
    return rv_name.test(name);
}

function isValidNumber(num) {
    var numRegex = /^\d+$/;
    return numRegex.test(num);
}



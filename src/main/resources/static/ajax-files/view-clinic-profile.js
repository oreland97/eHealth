$(document).ready(function () {
    var clinicId = getParameterByName("clinicId");
    console.log(clinicId);
    $('#booking-btn').click(function () {
        window.location.replace("booking?clinicId=" + clinicId);
    });

    $("#author-btn").click(function () {
        window.location.replace("/login");
    });

    $.ajax({
        url: '/api/clinic-info',
        type: 'get',
        data: {
            "clinicId": clinicId
        },
        success: function (data) {
            var wordsFromName = data.name.split(" ");
            for (var i = 0; i < wordsFromName.length; i++) {
                if (i % 2 !== 0) {
                    $('#clinicName').append("<span class=\"text-primary\">" + wordsFromName[i] + "</span>");
                }
                else {
                    $('#clinicName').append(wordsFromName[i]);
                }
                $('#clinicName').append(" ");
            }
            var phoneString = "";
            if (data.phone !== null && !isEmpty(data.phone)) {
                phoneString = "<br>Телефон:\xa0\xa0" + data.phone;
            }
            var urlString = "";
            if (data.url !== null && !isEmpty(data.url)) {
                urlString = "<br>Посилання на сайт:\xa0\xa0<a href=\"" + data.url + "\">" + data.url + "</a>";
            }
            var descriptionString = "";
            if (data.description !== null && !isEmpty(data.description)) {
                descriptionString = data.description;
            }
            $('#baseInfo').html("<br>" + data.address.region + "\xa0\xa0область\xa0\xa0·\xa0\xa0" + data.address.city + "\xa0\xa0·\xa0\xa0" +
                data.address.location + "<br>Спеціалізації\xa0:\xa0\xa0\xa0" + data.specializations.join("\xa0\xa0·\xa0\xa0") + "<br>" +
                "Час роботи:\xa0\xa0" + parseTime(data.schedule.timeOfStart) + "\xa0-\xa0" + parseTime(data.schedule.timeOfEnd) + phoneString + urlString);
            $('#clinicDescription').text(descriptionString).css("font-weight", "bold");

            var doctors = data.doctors;
            var doctorDivHtml = "";
            if (doctors.length !== 0) {
                for (var i = 0; i < doctors.length; i++) {
                    doctorDivHtml = "<div class=\"resume-item d-flex flex-column flex-md-row mb-5\" style='border: 5px solid #ffc107; background-color: #f8f4ff'>" +
                        "<div class=\"resume-content mr-auto\">" +
                        "<h3 class=\"mb-0\">\xa0\xa0\xa0\xa0" + doctors[i].firstName + "\xa0\xa0" + doctors[i].secondName + "</h3>" +
                        "<div class=\"subheading mb-3\" style='text-transform: capitalize'>\xa0\xa0\xa0\xa0\xa0" + doctors[i].specialization + "</div>" +
                        "<p>\xa0\xa0\xa0\xa0" + doctors[i].description + "</p>" + createTable(getStringSchedules(doctors[i].schedules)) +
                        "</div>" +
                        "<div class=\"resume-date text-md-right\">" +
                        "<span style='color: #2d458f'>" + "Тел. \xa0+" + doctors[i].phone + "\xa0\xa0\xa0\xa0</span>" +
                        "</div>" +
                        "</div>";
                    $('#doctorsInfo').append(doctorDivHtml);
                }
            }
            var services = data.services;
            var serviceDivHtml = "";
            if (services.length !== 0 && services[0].name !== null) {
                for (var i = 0; i < services.length; i++) {
                    serviceDivHtml = "<div class=\"resume-item d-flex flex-column flex-md-row mb-5\" style='border: 5px solid #ffc107; background-color: #f8f4ff'>" +
                        "<div class=\"resume-content mr-auto\">" +
                        "<h3 class=\"mb-0\">" + services[i].name + "</h3>" +
                        "</div>" +
                        "<div class=\"resume-date text-md-right\">" +
                        "<span style='color: #2d458f'>" + services[i].price + " грн." + "</span>" +
                        "</div>" +
                        "</div>";
                    $('#servicesInfo').append(serviceDivHtml);
                }
            }
            var feedbacks = data.feedbacks;
            if (feedbacks.length !== 0) {
                for (var i = 0; i < feedbacks.length; i++) {
                    var feedbackDivHtml = "<div class=\"resume-item d-flex flex-column flex-md-row mb-5\">" +
                        "<div class=\"resume-content mr-auto\" style='background-color: #ffc107'>" +
                        "<h3 class=\"mb-0\">\xa0" + feedbacks[i].patient.firstName + " " + feedbacks[i].patient.secondName +
                        "<span style='float: right'>" + parseDateTime(feedbacks[i].dateTime) + "\xa0\xa0</span></h3>" +
                        "\xa0<span class=\"spanMain" + i + "\"><span class=\"fa fa-star span0\"></span>" +
                        "<span class=\"fa fa-star span1\"></span>" +
                        "<span class=\"fa fa-star span2\"></span>" +
                        "<span class=\"fa fa-star span3\"></span>" +
                        "<span class=\"fa fa-star span4\"></span></span>" +
                        "<br>\xa0<textarea style='resize: none; border: 0; ' cols='100' rows='5' readonly id='textarea'>" +
                        feedbacks[i].description + "</textarea>\xa0</div></div>";
                    $('#feedbacksInfo').append(feedbackDivHtml);
                    for (var j = 0; j < feedbacks[i].rating; j++) {
                        $('.spanMain' + i + ' .span' + j).css('color', '#0e62c7');
                    }
                }
            }
        }
    });

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    function isEmpty(str) {
        return str.trim() === '';
    }

    function getStringSchedules(schedules) {
        var schedule = {};
        for (var i = 0; i < schedules.length; i++) {
            if (schedules[i].dayOfWeek === null) {
                return null;
            }
            schedule[schedules[i].dayOfWeek] = parseTime(schedules[i].timeOfStart) + " - " + parseTime(schedules[i].timeOfEnd);
        }
        console.log(schedule);
        return schedule;
    }

    function parseTime(time) {
        return time.substring(0, time.length - 3);
    }

    function parseDateTime(dateTime) {
        var date = new Date(dateTime);
        return date.toLocaleString();
    }

    function createTable(object) {
        var content = "<table style='text-align: center; margin-left: 15px' cellpadding='10' rules='cols'><tr>";
        for (var i = 0; i < getKeys(object).length; i++) {
            content += "<th>" + parseDaysOfWeekIntoUkr(getKeys(object))[i] + "</th>";
        }
        content += "</tr><tr>";
        for (var i = 0; i < getKeys(object).length; i++) {
            content += '<td>' + object[getKeys(object)[i]] + '</td>';
        }
        content += "</tr></table>";
        return content;
    }

    function getKeys(obj) {
        var keys = [];
        for (var key in obj) {
            keys.push(key);
        }
        return keys;
    }

    function parseDaysOfWeekIntoUkr(engDays) {
        var ukrDays = [];
        for (var i = 0; i < engDays.length; i++) {
            if (engDays[i].toLowerCase() === "monday") {
                ukrDays[i] = "Понеділок";
            }
            else if (engDays[i].toLowerCase() === "tuesday") {
                ukrDays[i] = "Вівторок";
            }
            else if (engDays[i].toLowerCase() === "wednesday") {
                ukrDays[i] = "Середа";
            }
            else if (engDays[i].toLowerCase() === "thursday") {
                ukrDays[i] = "Четвер";
            }
            else if (engDays[i].toLowerCase() === "friday") {
                ukrDays[i] = "П'ятниця";
            }
            else if (engDays[i].toLowerCase() === "saturday") {
                ukrDays[i] = "Субота";
            }
            else if (engDays[i].toLowerCase() === "sunday") {
                ukrDays[i] = "Неділя";
            }
        }
        ukrDays.sort(uaSort);

        return ukrDays;
    }

    function uaSort(s1, s2) {
        var ukrDays = ["Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця", "Субота", "Неділя"];
        var index1, index2;
        for (var i = 0; i < ukrDays.length; i++) {
            if (s1 === ukrDays[i]) {
                index1 = i;
            }
            if (s2 === ukrDays[i]) {
                index2 = i;
            }
        }
        return index1 - index2;
    }
});
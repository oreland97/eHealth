$(document).ready(function () {
    $('#password_pat, #confirm_password_pat').on('keyup', function () {
        if ($('#password_pat').val() === $('#confirm_password_pat').val()) {
            $('#passwordMessage_pat').html('Пароль підтверджено!').css('color', 'green');
            $('#submit_patient').prop('disabled', false);
        }
        else {
            $('#passwordMessage_pat').html('Пароль не однаковий!').css('color', 'red');
            $('#submit_patient').prop('disabled', true);
        }
    });

    $('#email_pat').on('keyup', function () {
        var email = $('#email_pat').val();
        if (isEmail(email)) {
            $('#submit_patient').prop('disabled', false);
            $('#email_patient_Message').html('');
        }
        else {
            $('#email_patient_Message').html('Зайві символи в email!').css('color', 'red');
            $('#submit_patient').prop('disabled', true);
        }
    });

    $('#first_name').on('keyup', function () {
       var firstName = $('#first_name').val();
       if (isValidNames(firstName)) {
           $('#submit_patient').prop('disabled', false);
           $('#fName_patient_Message').html('');
       }
       else {
           $('#fName_patient_Message').html('Зайві символи в імені!').css('color', 'red');
           $('#submit_patient').prop('disabled', true);
       }
    });

    $('#second_name').on('keyup', function () {
        var secondName = $('#second_name').val();
        if (isValidNames(secondName)) {
            $('#submit_patient').prop('disabled', false);
            $('#sName_patient_Message').html('');
        }
        else {
            $('#sName_patient_Message').html('Зайві символи в прізвищі!').css('color', 'red');
            $('#submit_patient').prop('disabled', true);
        }
    });

    $("#author-btn").click(function () {
        window.location.replace("/login");
    });

    $('#submit_patient').click(function () {
        var firstName = $('#first_name').val();
        var secondName = $('#second_name').val();
        var inputEmail = $('#email_pat').val();
        var inputPassword = $('#password_pat').val();

        if (firstName !== '' && secondName !== '' &&
            inputEmail !== '' && inputPassword !== '') {
            $.ajax({
                url: "api/patient/sign_up",
                type: "POST",
                data: {
                    "firstName": firstName,
                    "secondName": secondName,
                    "email": inputEmail,
                    "password": inputPassword
                },
                statusCode: {
                    200: function (data) {
                        window.location.replace("patient-edit");
                    },
                    401: function (data) {
                        $('#error_reg_pat').html("<strong>Error:</strong> Такий профіль вже існує!");
                        $('.container').find('input:password').val('');
                        $('#error_reg_pat').fadeIn().delay(3000).fadeOut();
                    }
                }
            });
            return false;
        }
    });
});

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function isValidNames(name) {
    var rv_name = /^[А-Яа-яЁёЇїІіЄєҐґ'.,"\s\d-]+$/;
    return rv_name.test(name);
}
$(document).ready(function () {

    function setTrimmedFeedbackText(feedbackElem, feedbackFullText) {
        var MAX_NOT_TRIMMED_FEEDBACK_LENGTH = 50;
        var feedbackColumnText = feedbackFullText;
        if (feedbackFullText.length > MAX_NOT_TRIMMED_FEEDBACK_LENGTH) {
            feedbackElem.html(feedbackFullText.substring(0, MAX_NOT_TRIMMED_FEEDBACK_LENGTH));

            var feedbackMoreLinkTemplate = $(".feedbackMoreLinkTemplate").clone();
            feedbackMoreLinkTemplate.removeClass("feedbackMoreLinkTemplate");
            feedbackMoreLinkTemplate.click(function() {
                $("#feedback_text_modal .feedbackText").html(feedbackFullText);
            });

            feedbackElem.append(feedbackMoreLinkTemplate);
        } else {
            feedbackElem.html(feedbackFullText);
        }
    }

    var role = getRole();
    if (role === "PATIENT") {
        initModalWindowStarRating();
    }

    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "api/records",
        dataType: 'json',
        success: function (data) {
            $.each(data, function (index, record) {
                var recordTemplate = $(".recordTemplate").clone().show();
                recordTemplate.removeClass("recordTemplate");

                var status;
                if (record.status === 'INCOMPLETE') {
                    status = 'Не завершена';
                }
                if (record.status === 'COMPLETE') {
                    status = 'Завершена';
                }
                if (record.status === 'CANCELED') {
                    status = 'Скасована';
                }

                recordTemplate.find(".recordDate").html(record.date);
                recordTemplate.find(".recordTime").html(record.time);

                if (record.patient) {
                    recordTemplate.find(".recordPatient").html(record.patient.firstName + " " + record.patient.secondName);
                }
                recordTemplate.find(".recordDoctor").html(record.doctor.firstName + " " + record.doctor.secondName);
                recordTemplate.find(".recordService").html(record.service.name);
                recordTemplate.find(".recordStatus").html(status);


                var disableCancelAction = record.status !== 'INCOMPLETE';
                var cancelButton = createUpdateStatusButton("Відмінити", "canceled", disableCancelAction, record.id);
                recordTemplate.find(".recordCancel").html(cancelButton);

                var starredFeedbackSection = $(".starredFeedbackTemplate").clone();
                starredFeedbackSection.removeClass("starredFeedbackTemplate");
                if (record.feedback) {
                    setTrimmedFeedbackText(starredFeedbackSection.find(".feedbackText"), record.feedback.description);
                    starredFeedbackSection.find(".star-rating .rating-value").val(record.feedback.rating);
                    setRatingStar(starredFeedbackSection.find(".star-rating .fa"));
                }

                recordTemplate.find(".recordFeedback").html(starredFeedbackSection);

                if (role === 'PATIENT') {
                    if (!record.feedback) {
                        var disableFeedback = record.status !== 'COMPLETE';
                        var feedbackButton = createFeedbackButton(disableFeedback, record.id);
                        recordTemplate.find(".recordFeedback").html(feedbackButton);
                    }
                } else if (role === 'CLINIC') {
                    var disableCompleteAction = record.status !== 'INCOMPLETE';
                    var completeButton = createUpdateStatusButton("Завершити", "complete", disableCompleteAction, record.id);
                    recordTemplate.find(".recordComplete").html(completeButton);
                }


                $("#clinic-records").append(recordTemplate);
            });

            if (role === "PATIENT") {
                $(".patientCells").hide();
                $(".completeButtonCells").hide();
            }
        }
    });
});

function getRole() {
    return document.getElementById("role").value;
}

function setRatingStar(starRatingSection) {
    return starRatingSection.each(function () {
        if (parseInt(starRatingSection.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
            return $(this).removeClass('fa-star-o').addClass('fa-star');
        } else {
            return $(this).removeClass('fa-star').addClass('fa-star-o');
        }
    });
}

function initModalWindowStarRating() {
    var starRating = $('.feedbackWindow .star-rating .fa');
    starRating.on('click', function () {
        starRating.siblings('input.rating-value').val($(this).data('rating'));
        return setRatingStar(starRating);
    });

    setRatingStar(starRating);
}

function createUpdateStatusButton(buttonName, status, isDisabled, recordId) {
    var buttonTemplate = $(".buttonTemplate").clone();
    buttonTemplate.removeClass("buttonTemplate");
    console.log(status);

    if (isDisabled) {
        buttonTemplate.addClass("disabled");
    } else {
        buttonTemplate.click(function () {
            var updateStatusData = {
                "status": status,
                "recordId": recordId
            };

            $.ajax({
                url: 'api/records/update-status',
                type: 'POST',
                dataType: 'json',
                contentType: "application/json",
                data: JSON.stringify(updateStatusData),
                statusCode: {
                    200: function (data) {
                        location.reload();
                    }
                }
            });
        });
    }
    buttonTemplate.html(buttonName);

    return buttonTemplate;
}

function createFeedbackButton(isDisabled, recordId) {
    var feedbackButtonTemplate = $(".addFeedbackTemplate").clone();
    feedbackButtonTemplate.removeClass("addFeedbackTemplate");
    feedbackButtonTemplate.val(recordId);

    if (isDisabled) {
        feedbackButtonTemplate.addClass("disabled");
    } else {
        feedbackButtonTemplate.click(function () {
            $("#feedback-save").click(function () {
                var feedbackData = {
                    "rating": $(".feedbackWindow .rating-value").val(),
                    "description": $(".feedbackWindow .feedbackDescription").val(),
                    "idRecord": recordId
                };

                $.ajax({
                    url: 'api/records/add-feedback',
                    type: 'POST',
                    dataType: 'json',
                    contentType: "application/json",
                    data: JSON.stringify(feedbackData),
                    statusCode: {
                        200: function (data) {
                            location.reload();
                        }
                    }
                });
            });
        });
        return feedbackButtonTemplate;
    }
}

$(document).ready(function () {

    var clinicId = getParameterByName("clinicId");

    var fridayBooking = function () {
        modal({
            type: 'error', //Type of Modal Box (alert | confirm | prompt | success | warning | error | info | inverted | primary)
            title: 'Запис неможливий', //Modal Title
            text: 'Вибачте, але сьогодні неможливо записатись на прийом, так як немає ще доступного графіку лікарів. Почекайте до завтра, на ранок все буде готово.', //Modal HTML Content
            size: 'normal', //Modal Size (normal | large | small)
            buttons: [{
                text: 'OK', //Button Text
                val: 'ok', //Button Value
                eKey: false, //Enter Keypress
                addClass: 'btn-light-blue', //Button Classes (btn-large | btn-small | btn-green | btn-light-green | btn-purple | btn-orange | btn-pink | btn-turquoise | btn-blue | btn-light-blue | btn-light-red | btn-red | btn-yellow | btn-white | btn-black | btn-rounded | btn-circle | btn-square | btn-disabled)
                onClick: function () {
                    window.location.replace("/search");
                }
            }],
            center: true, //Center Modal Box?
            autoclose: false, //Auto Close Modal Box?
            callback: null, //Callback Function after close Modal (ex: function(result){alert(result); return true;})
            onShow: function (r) {
            }, //After show Modal function
            closeClick: false, //Close Modal on click near the box
            closable: true, //If Modal is closable
            theme: 'atlant', //Modal Custom Theme (xenon | atlant | reseted)
            animate: true, //Slide animation
            background: 'rgba(0,0,0,0.35)', //Background Color, it can be null
            zIndex: 1050, //z-index
            buttonText: {
                ok: 'OK',
                yes: 'Yes',
                cancel: 'Cancel'
            },
            template: '<div class="modal-box"><div class="modal-inner"><div class="modal-title"><a class="modal-close-btn"></a></div><div class="modal-text"></div><div class="modal-buttons"></div></div></div>',
            _classes: {
                box: '.modal-box',
                boxInner: ".modal-inner",
                title: '.modal-title',
                content: '.modal-text',
                buttons: '.modal-buttons',
                closebtn: '.modal-close-btn'
            }
        });
    };

    if (new Date().getDay() === 5) {
        fridayBooking();
    }

    var serviceId;
    var doctorId;
    var day;
    var time;
    var doctors = [];
    var services = [];


    function parseDaysOfWeekIntoUkr(engDays) {
        var ukrDays = [];
        for (var i = 0; i < engDays.length; i++) {
            if (engDays[i].toLowerCase() === "monday") {
                ukrDays[i] = "Понеділок";
            }
            else if (engDays[i].toLowerCase() === "tuesday") {
                ukrDays[i] = "Вівторок";
            }
            else if (engDays[i].toLowerCase() === "wednesday") {
                ukrDays[i] = "Середа";
            }
            else if (engDays[i].toLowerCase() === "thursday") {
                ukrDays[i] = "Четвер";
            }
            else if (engDays[i].toLowerCase() === "friday") {
                ukrDays[i] = "П'ятниця";
            }
            else if (engDays[i].toLowerCase() === "saturday") {
                ukrDays[i] = "Субота";
            }
            else if (engDays[i].toLowerCase() === "sunday") {
                ukrDays[i] = "Неділя";
            }
        }
        ukrDays.sort(uaSort);

        return ukrDays;
    }

    function parseDayOfWeekIntoEng(ukrDay) {
        var engDay;
        if (ukrDay.toLowerCase() === "понеділок") {
            engDay = "monday";
        }
        else if (ukrDay.toLowerCase() === "вівторок") {
            engDay = "tuesday";
        }
        else if (ukrDay.toLowerCase() === "середа") {
            engDay = "wednesday";
        }
        else if (ukrDay.toLowerCase() === "четвер") {
            engDay = "thursday";
        }
        else if (ukrDay.toLowerCase() === "п'ятниця") {
            engDay = "friday";
        }
        else if (ukrDay.toLowerCase() === "субота") {
            engDay = "saturday";
        }
        else if (ukrDay.toLowerCase() === "неділя") {
            engDay = "sunday";
        }
        return engDay;
    }

    function uaSort(s1, s2) {
        var ukrDays = ["Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця", "Субота", "Неділя"];
        var index1, index2;
        for (var i = 0; i < ukrDays.length; i++) {
            if (s1 === ukrDays[i]) {
                index1 = i;
            }
            if (s2 === ukrDays[i]) {
                index2 = i;
            }
        }
        return index1 - index2;
    }


    var maxLength = 800;
    $('#diseaseDescription').keyup(function () {
        var length = $(this).val().length;
        var length = maxLength - length;
        $('#chars').text(length);
    });


    $.ajax({
        type: "get",
        url: "api/booking/services",
        cache: false,
        dataType: "json",
        data: {
            "clinicId": clinicId
        },
        success: function (response) {
            $.each(response, function (index) {
                services[index] = response;
                var option;
                var selectedService = document.getElementById("service").value;
                if (selectedService === response[index]) {
                    option = new Option(response[index].name, response[index].id, true, true);
                    $(option).html(response[index].name);
                    $('#service').append(option);
                } else {
                    option = new Option(response[index].name);
                    option.setAttribute('value', response[index].id);
                    $(option).html(response[index].name);
                    $('#service').append(option)
                }
            });

            $('#service').on('change', function () {
                serviceId = document.getElementById("service").value;
                document.getElementById('step1_next').style.visibility = "visible";
                $.ajax({
                    type: "get",
                    url: "api/booking/doctors",
                    cache: false,
                    dataType: "json",
                    data: {
                        "clinicId": clinicId,
                        "serviceId": serviceId
                    },
                    success: function (response) {
                        if (response.length == 0) {
                            servicesEmpty();
                        }
                        $('#doctor').empty();
                        var defDoctor = new Option("Виберіть лікаря...");
                        defDoctor.setAttribute('disabled', 'disabled');
                        defDoctor.setAttribute('selected', 'selected');
                        $(defDoctor).html("Виберіть лікаря...");
                        $('#doctor').append(defDoctor);
                        $.each(response, function (index) {
                            doctors[index] = response;
                            var option;
                            var selectedService = document.getElementById("doctor").value;
                            if (selectedService === response[index]) {
                                option = new Option(response[index].firstName + " " + response[index].secondName, response[index].id, true, true);
                                $(option).html(response[index].firstName + " " + response[index].secondName);
                                $('#doctor').append(option);
                            } else {
                                option = new Option(response[index].firstName + " " + response[index].secondName);
                                option.setAttribute('value', response[index].id);
                                $(option).html(response[index].firstName + " " + response[index].secondName);
                                $('#doctor').append(option)
                            }
                        });
                        if ($('#doctor option:selected').text() === 'Виберіть лікаря...') {
                            document.getElementById('step2_next').style.visibility = "hidden";
                        }

                        $('#doctor').on('change', function () {
                            document.getElementById('step2_next').style.visibility = "visible";
                            doctorId = document.getElementById("doctor").value;
                            $.ajax({
                                type: "get",
                                url: "api/booking/dates",
                                cache: false,
                                dataType: "json",
                                data: {
                                    "doctorId": doctorId
                                },
                                success: function (response) {
                                    if (response.length == 0) {
                                        doctorsEmpty();
                                    }
                                    $('#date').empty();
                                    var defDay = new Option("Виберіть день...");
                                    defDay.setAttribute('disabled', 'disabled');
                                    defDay.setAttribute('selected', 'selected');
                                    $(defDay).html("Виберіть день...");
                                    var days = parseDaysOfWeekIntoUkr(response);
                                    $('#date').append(defDay);
                                    $.each(days, function (index) {
                                        var option;
                                        var selectedService = document.getElementById("date").value;
                                        if (selectedService === days[index]) {
                                            option = new Option(days[index], days[index], true, true);
                                            $(option).html(days[index]);
                                            $('#date').append(option);
                                        } else {
                                            option = new Option(days[index]);
                                            option.setAttribute('value', days[index]);
                                            $(option).html(days[index]);
                                            $('#date').append(option)
                                        }
                                    });
                                    if ($('#date option:selected').text() === 'Виберіть день...') {
                                        document.getElementById('step3_next').style.visibility = "hidden";
                                    }

                                    $('#date').on('change', function () {
                                        document.getElementById('step3_next').style.visibility = "visible";
                                        day = $('#date option:selected').text();
                                        $.ajax({
                                            type: "get",
                                            url: "api/booking/times",
                                            cache: false,
                                            dataType: "json",
                                            data: {
                                                "day": parseDayOfWeekIntoEng(day),
                                                "doctorId": doctorId
                                            },
                                            success: function (response) {
                                                if (response.length == 0) {
                                                    dateTimeEmpty();
                                                }
                                                $('#time').empty();
                                                var defTime = new Option("Виберіть час...");
                                                defTime.setAttribute('disabled', 'disabled');
                                                defTime.setAttribute('selected', 'selected');
                                                $(defTime).html("Виберіть час...");
                                                $('#time').append(defTime);
                                                $.each(response, function (index) {
                                                    var option;
                                                    var selectedService = document.getElementById("time").value;
                                                    if (selectedService === response[index]) {
                                                        option = new Option(response[index], response[index], true, true);
                                                        $(option).html(response[index]);
                                                        $('#time').append(option);
                                                    } else {
                                                        option = new Option(response[index]);
                                                        option.setAttribute('value', response[index]);
                                                        $(option).html(response[index]);
                                                        $('#time').append(option)
                                                    }
                                                });
                                                if ($('#time option:selected').text() === 'Виберіть час...') {
                                                    document.getElementById('step4_next').style.visibility = "hidden";
                                                }

                                                $('#time').on('change', function () {
                                                    document.getElementById('step4_next').style.visibility = "visible";
                                                    time = $('#time option:selected').text();
                                                });
                                            }
                                        });
                                    });
                                }
                            });

                        });
                    }
                });
            });
        }
    });

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $('#step5_next').click(function () {
        var service;
        var doctor;
        for (var i = 0; i < services.length; i++) {
            if (services[0][i].id == serviceId) {
                service = services[0][i];
                console.log(service);
                break;
            }
        }
        for (var i = 0; i < doctors.length; i++) {
            if (doctors[0][i].id == doctorId) {
                doctor = doctors[0][i];
                console.log(doctor);
                break;
            }
        }
        var divInfo = "<div><h3>Послуга<hr></h3><br><h5>Назва - " + service.name + "<br>Ціна - " + service.price + " грн.</h5><hr class='style'><br>" +
            "<h3>Лікар<hr></h3><br><h5>Ініціали - " + doctor.firstName + " " + doctor.secondName + "<br>" +
            "Спеціалізація - " + doctor.specialization + "</h5><hr class='style'><br><h3>День/час<hr></h3><br><h5>День - " + day + "<br>Час - " + time + "</h5></div>";
        $('#confirmInfo').html(divInfo);
    });

    $('#submit-booking').click(function () {
        var inputDoctor = doctorId;
        var inputService = serviceId;
        var inputTime = time;
        var inputDay = parseDayOfWeekIntoEng(day);
        $.ajax({
            type: "POST",
            url: "api/booking",
            data: {
                "doctorId": inputDoctor,
                "serviceId": inputService,
                "time": inputTime,
                "clinicId": clinicId,
                "nameOfDay": inputDay
            },
            statusCode: {
                200: function () {
                    modal({
                        type: 'success', //Type of Modal Box (alert | confirm | prompt | success | warning | error | info | inverted | primary)
                        title: 'Запис до лікаря', //Modal Title
                        text: 'Ви успішно записались на прийом до лікаря. Будемо Вас чекати!', //Modal HTML Content
                        size: 'normal', //Modal Size (normal | large | small)
                        buttons: [{
                            text: 'OK', //Button Text
                            val: 'ok', //Button Value
                            eKey: false, //Enter Keypress
                            addClass: 'btn-light-blue', //Button Classes (btn-large | btn-small | btn-green | btn-light-green | btn-purple | btn-orange | btn-pink | btn-turquoise | btn-blue | btn-light-blue | btn-light-red | btn-red | btn-yellow | btn-white | btn-black | btn-rounded | btn-circle | btn-square | btn-disabled)
                            onClick: function () {
                                window.location.replace("/search");
                            }
                        }],
                        center: true, //Center Modal Box?
                        autoclose: false, //Auto Close Modal Box?
                        callback: null, //Callback Function after close Modal (ex: function(result){alert(result); return true;})
                        onShow: function (r) {
                        }, //After show Modal function
                        closeClick: false, //Close Modal on click near the box
                        closable: true, //If Modal is closable
                        theme: 'atlant', //Modal Custom Theme (xenon | atlant | reseted)
                        animate: true, //Slide animation
                        background: 'rgba(0,0,0,0.35)', //Background Color, it can be null
                        zIndex: 1050, //z-index
                        buttonText: {
                            ok: 'OK',
                            yes: 'Yes',
                            cancel: 'Cancel'
                        },
                        template: '<div class="modal-box"><div class="modal-inner"><div class="modal-title"><a class="modal-close-btn"></a></div><div class="modal-text"></div><div class="modal-buttons"></div></div></div>',
                        _classes: {
                            box: '.modal-box',
                            boxInner: ".modal-inner",
                            title: '.modal-title',
                            content: '.modal-text',
                            buttons: '.modal-buttons',
                            closebtn: '.modal-close-btn'
                        }
                    });
                }
            },
            error: function () {
                modal({
                    type: 'error', //Type of Modal Box (alert | confirm | prompt | success | warning | error | info | inverted | primary)
                    title: 'Запис до лікаря', //Modal Title
                    text: 'Йой, якась помилка. Напевно серйозна. Натисніть ОК щоб ми зрозуміли шо Вам пофіг.', //Modal HTML Content
                    size: 'normal', //Modal Size (normal | large | small)
                    buttons: [{
                        text: 'OK', //Button Text
                        val: 'ok', //Button Value
                        eKey: false, //Enter Keypress
                        addClass: 'btn-red' //Button Classes (btn-large | btn-small | btn-green | btn-light-green | btn-purple | btn-orange | btn-pink | btn-turquoise | btn-blue | btn-light-blue | btn-light-red | btn-red | btn-yellow | btn-white | btn-black | btn-rounded | btn-circle | btn-square | btn-disabled)
                    }],
                    center: true, //Center Modal Box?
                    autoclose: false, //Auto Close Modal Box?
                    callback: null, //Callback Function after close Modal (ex: function(result){alert(result); return true;})
                    onShow: function (r) {
                    }, //After show Modal function
                    closeClick: false, //Close Modal on click near the box
                    closable: true, //If Modal is closable
                    theme: 'atlant', //Modal Custom Theme (xenon | atlant | reseted)
                    animate: true, //Slide animation
                    background: 'rgba(0,0,0,0.35)', //Background Color, it can be null
                    zIndex: 1050, //z-index
                    buttonText: {
                        ok: 'OK',
                        yes: 'Yes',
                        cancel: 'Cancel'
                    },
                    template: '<div class="modal-box"><div class="modal-inner"><div class="modal-title"><a class="modal-close-btn"></a></div><div class="modal-text"></div><div class="modal-buttons"></div></div></div>',
                    _classes: {
                        box: '.modal-box',
                        boxInner: ".modal-inner",
                        title: '.modal-title',
                        content: '.modal-text',
                        buttons: '.modal-buttons',
                        closebtn: '.modal-close-btn'
                    }
                });
            }
        });
    });


    var servicesEmpty = function () {
        modal({
            type: 'warning', //Type of Modal Box (alert | confirm | prompt | success | warning | error | info | inverted | primary)
            title: 'Немає послуг', //Modal Title
            text: 'У даної клініки ще немає ніяких послуг. Вибачте за незручності.', //Modal HTML Content
            size: 'normal', //Modal Size (normal | large | small)
            buttons: [{
                text: 'OK', //Button Text
                val: 'ok', //Button Value
                eKey: false, //Enter Keypress
                addClass: 'btn-light-blue', //Button Classes (btn-large | btn-small | btn-green | btn-light-green | btn-purple | btn-orange | btn-pink | btn-turquoise | btn-blue | btn-light-blue | btn-light-red | btn-red | btn-yellow | btn-white | btn-black | btn-rounded | btn-circle | btn-square | btn-disabled)
                onClick: function () {
                    window.location.replace("/search");
                }
            }],
            center: true, //Center Modal Box?
            autoclose: false, //Auto Close Modal Box?
            callback: null, //Callback Function after close Modal (ex: function(result){alert(result); return true;})
            onShow: function (r) {
            }, //After show Modal function
            closeClick: false, //Close Modal on click near the box
            closable: true, //If Modal is closable
            theme: 'atlant', //Modal Custom Theme (xenon | atlant | reseted)
            animate: true, //Slide animation
            background: 'rgba(0,0,0,0.35)', //Background Color, it can be null
            zIndex: 1050, //z-index
            buttonText: {
                ok: 'OK',
                yes: 'Yes',
                cancel: 'Cancel'
            },
            template: '<div class="modal-box"><div class="modal-inner"><div class="modal-title"><a class="modal-close-btn"></a></div><div class="modal-text"></div><div class="modal-buttons"></div></div></div>',
            _classes: {
                box: '.modal-box',
                boxInner: ".modal-inner",
                title: '.modal-title',
                content: '.modal-text',
                buttons: '.modal-buttons',
                closebtn: '.modal-close-btn'
            }
        });
    };

    var doctorsEmpty = function () {
        modal({
            type: 'alert', //Type of Modal Box (alert | confirm | prompt | success | warning | error | info | inverted | primary)
            title: 'Лікар зайнятий', //Modal Title
            text: 'Вибраний лікар зайнятий, виберіть іншого.', //Modal HTML Content
            size: 'normal', //Modal Size (normal | large | small)
            buttons: [{
                text: 'OK', //Button Text
                val: 'ok', //Button Value
                eKey: false, //Enter Keypress
                addClass: 'btn-light-blue', //Button Classes (btn-large | btn-small | btn-green | btn-light-green | btn-purple | btn-orange | btn-pink | btn-turquoise | btn-blue | btn-light-blue | btn-light-red | btn-red | btn-yellow | btn-white | btn-black | btn-rounded | btn-circle | btn-square | btn-disabled)
                onClick: function () {

                }
            }],
            center: true, //Center Modal Box?
            autoclose: false, //Auto Close Modal Box?
            callback: null, //Callback Function after close Modal (ex: function(result){alert(result); return true;})
            onShow: function (r) {
            }, //After show Modal function
            closeClick: false, //Close Modal on click near the box
            closable: true, //If Modal is closable
            theme: 'atlant', //Modal Custom Theme (xenon | atlant | reseted)
            animate: true, //Slide animation
            background: 'rgba(0,0,0,0.35)', //Background Color, it can be null
            zIndex: 1050, //z-index
            buttonText: {
                ok: 'OK',
                yes: 'Yes',
                cancel: 'Cancel'
            },
            template: '<div class="modal-box"><div class="modal-inner"><div class="modal-title"><a class="modal-close-btn"></a></div><div class="modal-text"></div><div class="modal-buttons"></div></div></div>',
            _classes: {
                box: '.modal-box',
                boxInner: ".modal-inner",
                title: '.modal-title',
                content: '.modal-text',
                buttons: '.modal-buttons',
                closebtn: '.modal-close-btn'
            }
        });
    };

    var dateTimeEmpty = function () {
        modal({
            type: 'alert', //Type of Modal Box (alert | confirm | prompt | success | warning | error | info | inverted | primary)
            title: 'Немає лікарів', //Modal Title
            text: 'На вибраний час лікар не зможе Вас прийняти. Виберіть інший час.', //Modal HTML Content
            size: 'normal', //Modal Size (normal | large | small)
            buttons: [{
                text: 'OK', //Button Text
                val: 'ok', //Button Value
                eKey: false, //Enter Keypress
                addClass: 'btn-light-blue', //Button Classes (btn-large | btn-small | btn-green | btn-light-green | btn-purple | btn-orange | btn-pink | btn-turquoise | btn-blue | btn-light-blue | btn-light-red | btn-red | btn-yellow | btn-white | btn-black | btn-rounded | btn-circle | btn-square | btn-disabled)
                onClick: function () {

                }
            }],
            center: true, //Center Modal Box?
            autoclose: false, //Auto Close Modal Box?
            callback: null, //Callback Function after close Modal (ex: function(result){alert(result); return true;})
            onShow: function (r) {
            }, //After show Modal function
            closeClick: false, //Close Modal on click near the box
            closable: true, //If Modal is closable
            theme: 'atlant', //Modal Custom Theme (xenon | atlant | reseted)
            animate: true, //Slide animation
            background: 'rgba(0,0,0,0.35)', //Background Color, it can be null
            zIndex: 1050, //z-index
            buttonText: {
                ok: 'OK',
                yes: 'Yes',
                cancel: 'Cancel'
            },
            template: '<div class="modal-box"><div class="modal-inner"><div class="modal-title"><a class="modal-close-btn"></a></div><div class="modal-text"></div><div class="modal-buttons"></div></div></div>',
            _classes: {
                box: '.modal-box',
                boxInner: ".modal-inner",
                title: '.modal-title',
                content: '.modal-text',
                buttons: '.modal-buttons',
                closebtn: '.modal-close-btn'
            }
        });
    };


// -------------------------
// NEXT STEP button
// -------------------------
    $('.button.next').click(function () {
        var $btn = $(this),
            $step = $btn.parents('.modal-body'),
            stepIndex = $step.index(),
            $pag = $('.modal-header span').eq(stepIndex);

        if (stepIndex === 5) {
            var value = $('#activation').val();
            if (value != key) {
                error($step, $pag);
            }
            else {
                nextStep($step, $pag);
            }
        }
        else {
            nextStep($step, $pag);
        }
    });


// -------------------------
// PREVIOUS STEP button
// -------------------------
    $('.button.previous').click(function () {
        var $btn = $(this),
            $step = $btn.parents('.modal-body'),
            stepIndex = $step.index(),
            $pag = $('.modal-header span').eq(stepIndex);

        previousStep($step, $pag);
    });


// -------------------------
// NEXT STEP function
// -------------------------
    function nextStep($step, $pag) {
        // animate the step out
        $step.addClass('animate-out-to-left');

        // animate the step in
        setTimeout(function () {
            $step.removeClass('animate-out-to-left is-showing')
                .next().addClass('animate-in-from-right');
            $pag.removeClass('is-active is-invalid').addClass('is-valid')
                .next().addClass('is-active');
        }, 600);

        // after the animation, adjust the classes
        setTimeout(function () {
            $step.next().removeClass('animate-in-from-right')
                .addClass('is-showing');
        }, 1200);
    }


// -------------------------
// PREVIOUS STEP function
// -------------------------
    function previousStep($step, $pag) {
        // animate the step out
        $step.addClass('animate-out-to-right');

        // animate the step in
        setTimeout(function () {
            $step.removeClass('animate-out-to-right is-showing')
                .prev().addClass('animate-in-from-left');
            $pag.removeClass('is-active is-valid is-invalid')
                .prev().removeClass('is-valid is-invalid').addClass('is-active');
        }, 600);

        // after the animation, adjust the classes
        setTimeout(function () {
            $step.prev().removeClass('animate-in-from-left')
                .addClass('is-showing');
        }, 1200);
    }


// -------------------------
// ERROR function
// -------------------------
    function error($step, $pag) {
        $('#activation').addClass('input-error shake');
        $pag.addClass('is-invalid');
        $('.message').html('\
		<div class="alert alert-danger">\
			<p class="icon">Error</p>\
			<p>This step simulates an incorrect input. For this codepen, it\'s the only field that is checked. Of course, on a real website every input will be checked!</p>\
			<p>Please enter <code>' + key + '</code> to proceed</p>\
		</div>\
	');

        // after the animation, adjust the classes
        setTimeout(function () {
            $('#activation').removeClass('shake');
        }, 500);
    }


// -------------------------
// Activate popover
// -------------------------
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover();
    });
});

$(document).ready(function () {
    var patientFromResponse;

    $.ajax({
        url: 'api/patient/profile',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            patientFromResponse = data;
            $('#input_first_name').val(data.firstName);
            $('#input_second_name').val(data.secondName);
            $('#input_patient_email').val(data.email);
            $('#input_patient_phone').val(data.phone);
        }
    });

    $('#input_patient_email').on('keyup', function () {
        var email = $('#input_patient_email').val();
        if (isEmail(email)){
            $('#save_patient').prop('disabled', false);
            $('#email_patient_prof_Message').html('');
        }
        else {
            $('#email_patient_prof_Message').html('Зайві символи в email!').css('color', 'red');
            $('#save_patient').prop('disabled', true);
        }
    });

    $('#input_first_name').on('keyup', function () {
        var fName = $('#input_first_name').val();
        if (isValidNames(fName) && fName.length < 20){
            $('#save_patient').prop('disabled', false);
            $('#fName_patient_prof_Message').html('');
        }
        else {
            $('#fName_patient_prof_Message').html('Зайві символи в імені!').css('color', 'red');
            $('#save_patient').prop('disabled', true);
        }
    });

    $('#input_second_name').on('keyup', function () {
        var sName = $('#input_second_name').val();
        if (isValidNames(sName) && sName.length < 20){
            $('#save_patient').prop('disabled', false);
            $('#sName_patient_prof_Message').html('');
        }
        else {
            $('#sName_patient_prof_Message').html('Зайві символи в прізвищі!').css('color', 'red');
            $('#save_patient').prop('disabled', true);
        }
    });

    $('#input_patient_phone').on('keyup', function () {
        var phone = $('#input_patient_phone').val();
        if (isValidNumber(phone) && phone.length < 10){
            $('#save_patient').prop('disabled', false);
            $('#phone_patient_prof_Message').html('');
        }
        else {
            $('#phone_patient_prof_Message').html('Зайві символи в номері телефону!').css('color', 'red');
            $('#save_patient').prop('disabled', true);
        }
    });

    $('#save_patient').click(function () {
        patientFromResponse.firstName = $('#input_first_name').val();
        patientFromResponse.secondName = $('#input_second_name').val();
        patientFromResponse.email = $('#input_patient_email').val();
        patientFromResponse.phone = $('#input_patient_phone').val();

        if (patientFromResponse.firstName !== '' && patientFromResponse.secondName !== '' &&
            patientFromResponse.email !== '') {
            $.ajax({
                url: 'api/patient/profile',
                type: 'POST',
                dataType: 'json',
                contentType: "application/json",
                data: JSON.stringify(patientFromResponse),
                statusCode: {
                    200: function (data) {
                        window.location.replace("/search");
                    }
                }
            });
            return false;
        }

    });

    $('#patient_cancel').click(function () {
        window.location.replace("/search");
    })
});

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function isValidNames(name) {
    var rv_name = /^[А-Яа-яЁёЇїІіЄєҐґ'.,"\s\d-]+$/;
    return rv_name.test(name);
}

function isValidNumber(num) {
    var numRegex = /^\d+$/;
    return numRegex.test(num);
}
$(document).ready(function(){

    $('#password, #confirm_password').on('keyup', function () {
        if ($('#password').val() === $('#confirm_password').val()){
            $('#passwordMessage').html('Пароль підтверджено!').css('color', 'green');
            $('#submit_clinic').prop('disabled', false);
        }
        else {
            $('#passwordMessage').html('Пароль не однаковий!').css('color', 'red');
            $('#submit_clinic').prop('disabled', true);
        }
    });

    $('#name').on('keyup', function () {
        var name = $('#name').val();
        if (isValidNames(name)) {
            $('#submit_clinic').prop('disabled', false);
            $('#name_clinic_Message').html('');
        }
        else {
            $('#name_clinic_Message').html('Зайві символи в назві клініки!').css('color', 'red');
            $('#submit_clinic').prop('disabled', true);
        }
    });

    $("#author-btn").click(function () {
        window.location.replace("/login");
    });

    $('#email').on('keyup', function () {
        var email = $('#email').val();
       if (isEmail(email)){
           $('#submit_clinic').prop('disabled', false);
           $('#emailMessage').html('');
       }
       else {
           $('#emailMessage').html('Зайві символи в email!').css('color', 'red');
           $('#submit_clinic').prop('disabled', true);
       }
    });

    $('#submit_clinic').click(function(){
        var inputName = $('#name').val();
        var inputEmail = $('#email').val();
        var inputPassword = $('#password').val();

        if (inputName !== '' && inputEmail !== '' && inputPassword !== '') {
            $.ajax({
                url: "api/clinic/sign_up",
                type: "POST",
                data: {
                    "name":inputName,
                    "email":inputEmail,
                    "password":inputPassword
                },
                statusCode: {
                    200: function (data) {
                        window.location.replace("/clinic-edit");
                    },
                    401: function (data) {
                        $('#error_reg').html("<strong>Error:</strong> Такий профіль вже існує!");
                        $('.container').find('input:password').val('');
                        $('#error_reg').fadeIn().delay(3000).fadeOut();
                    }
                }
            });
            return false;
        }
    });
});

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function isValidNames(name) {
    var rv_name = /^[А-Яа-яЁёЇїІіЄєҐґ'.,"\s\d-]+$/;
    return rv_name.test(name);
}
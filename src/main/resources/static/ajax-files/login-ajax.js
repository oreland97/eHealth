$(document).ready(function () {

    $('#inputEmail').on('keyup', function () {
        var email = $('#inputEmail').val();

        if (isEmail(email)) {
            $('#submit').prop('disabled', false);
            $('#email_error').html('');
        }
        else {
            $('#email_error').html('Зайві символи в email!');
            $('#submit').prop('disabled', true);
        }
    });

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
});


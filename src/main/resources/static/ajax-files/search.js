$(document).ready(function () {

    // $("a[href='#top']").click(function() {
    //     $("html, body").animate({ scrollTop: 0 }, "slow");
    //     return false;
    // });

    $("#author-btn").click(function () {
        window.location.replace("/login");
    });

    $.ajax({
        url: 'api/clinics/specializations',
        type: 'GET',
        success: function (response) {
            $('#inputSpecialization').empty();
            var defSpecialization = new Option("Виберіть спеціалізацію...");
            $(defSpecialization).html("Виберіть спеціалізацію...");
            $('#inputSpecialization').append(defSpecialization);

            var option;
            var selectedSpecialization = document.getElementById("inputSpecialization").value;
            for (var i = 0; i < response.length; i++) {
                if (selectedSpecialization === response[i]) {
                    option = new Option((response)[i], (response)[i], true, true);
                    $(option).html((response)[i]);
                    $('#inputSpecialization').append(option);
                }
                else {
                    option = new Option((response)[i]);
                    $(option).html((response)[i]);
                    $('#inputSpecialization').append(option);
                }
            }
        }
    });

    $.ajax({
        url: 'api/clinics/regions',
        type: 'GET',
        success: function (response) {
            $('#inputRegion').empty();
            var defRegion = new Option("Виберіть область...");
            $(defRegion).html("Виберіть область...");
            $('#inputRegion').append(defRegion);
            var option;
            var selectedRegion = document.getElementById("inputRegion").value;
            for (var i = 0; i < response.length; i++) {
                if (selectedRegion === response[i]) {
                    option = new Option((response)[i], (response)[i], true, true);
                    $(option).html((response)[i]);
                    $('#inputRegion').append(option);
                }
                else {
                    option = new Option((response)[i]);
                    $(option).html((response)[i]);
                    $('#inputRegion').append(option);
                }
            }
        }
    });

    function getCities() {
        var selectedRegion = $('#inputRegion').val();
        $.ajax({
            url: 'api/clinics/cities',
            type: 'GET',
            data: {
                "selectedRegion": selectedRegion
            },
            success: function (response) {
                $('#inputCity').empty();
                var defCity = new Option("Виберіть місто...");
                $(defCity).html("Виберіть місто...");
                $('#inputCity').append(defCity);
                var option;
                selectedCity = document.getElementById("inputCity").value;
                for (var i = 0; i < response.length; i++) {
                    if (selectedCity === response[i]) {
                        option = new Option((response)[i], (response)[i], true, true);
                        $(option).html((response)[i]);
                        $('#inputCity').append(option);
                    }
                    else {
                        option = new Option((response)[i]);
                        $(option).html((response)[i]);
                        $('#inputCity').append(option);
                    }
                }
            }
        });
    }

    $('#inputRegion').on('change', getCities);

    getCities();

    $('#searchSubmit').click(function () {
        $('html, body').animate({
            scrollTop: $(".clinicList").offset().top - $(".navbar").height() - 20
        }, 2000);
        var search = {};
        search["specializations"] = $("#inputSpecialization").val();
        search["regions"] = $("#inputRegion").val();
        search["cities"] = $("#inputCity").val();

        if (search["specializations"] === "Виберіть спеціалізацію...") {
            search["specializations"] = null;
        }
        if (search["regions"] === "Виберіть область...") {
            search["regions"] = null;
        }
        if (search["cities"] === "Виберіть місто..." || search["city"] === "") {
            search["cities"] = null;
        }

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "api/clinics/search",
            data: JSON.stringify(search),
            dataType: 'json',
            success: function (data) {
                $('.clinicList').empty();
                $.each(data, function (index, value) {
                    var clinicItemTemplate = $(".clinicItem.clinicItemTemplate").clone();
                    clinicItemTemplate.removeClass("clinicItemTemplate");

                    clinicItemTemplate.find(".clinicName").html(value.name);
                    clinicItemTemplate.find(".clinicSpecialization").html(value.specializations);

                    var addressString = value.address.region + ", " + value.address.city + ", " + value.address.location;
                    clinicItemTemplate.find(".clinicAddress").html(addressString);
                    clinicItemTemplate.find(".clinicDesc").html(value.description);

                    clinicItemTemplate.find(".profileLink").attr("href", "/clinic-info?clinicId=" + value.id);


                    $(".clinicList").append(clinicItemTemplate)
                });
            }
        });
    });
});

$(document).ready(function () {
    var clinicId;

    $.ajax({
        url: 'api/clinic/profile/services',
        type: 'GET',
        success: function (response) {
            clinicId = response.clinicId;
            $(function () {
                $.each(response, function (i, item) {
                    var trHTML = '<tr>' +
                        '<td>' + item.name + '</td>' +
                        '<td>' + item.price + '</td>' +
                        '<td id="doc_edit">\n' +
                        '            <p data-placement="top">\n' +
                        '                <button class="btn btn-danger" value="' + item.id + '" type="button" id="service_delete_btn" data-toggle="modal"\n' +
                        '                        data-target="#delete_service_modal">\n' +
                        '                    <span class="fa fa-times"></span>\n' +
                        '                </button>\n' +
                        '            </p>\n' +
                        '        </td>';
                    $('#services_table').append(trHTML);
                });
            });
        }
    });

    $('#add_service_modal').on('show.bs.modal', function (e) {

        $.ajax({
            url: 'api/clinic/profile/all-services',
            type: 'GET',
            success: function (response) {
                var defOptionForSpec = new Option("Оберіть послугу...");
                $(defOptionForSpec).val('').html("Оберіть послугу...");
                $('#service_new_select').append(defOptionForSpec);
                var option;
                var selectedSpec = document.getElementById("service_new_select").value;
                for (var i = 0; i < response.length; i++) {
                    if (selectedSpec === response[i].id) {
                        option = new Option(response[i].name, response[i].id, true, true);
                        $(option).text((response[i].name));
                        $(option).val((response[i].id));
                        $('#service_new_select').append(option);
                    }
                    else {
                        option = new Option((response));
                        $(option).text((response[i].name));
                        $(option).val((response[i].id));
                        $('#service_new_select').append(option);
                    }
                }

            }
        });

        $('#service_price').on('keyup', function () {
            var price = $('#service_price').val();
            if (isValidPrice(price) && price.length < 10){
                $('#save_new_service').prop('disabled', false);
                $('#new_service_price_Message').html('');
            }
            else {
                $('#new_service_price_Message').html('Зайві символи в ціні послуги!').css('color', 'red');
                $('#save_new_service').prop('disabled', true);
            }
        });

        $('#save_new_service').click(function (e) {
            var serviceObj = {
                "clinicId": clinicId,
                "name": $('#service_new_select').val(),
                "price": $('#service_price').val()
            };

            if (serviceObj.name !== '' && serviceObj.price !== '') {
                $.ajax({
                    url: 'api/clinic/profile/add-service',
                    type: 'POST',
                    dataType: 'json',
                    contentType: "application/json",
                    data: JSON.stringify(serviceObj),
                    statusCode: {
                        200: function (data) {
                            window.location.replace("/services");
                        },
                        400: function (data) {
                            $('#error_service').html("<strong>Error:</strong> В клініці вже є така послуга!");
                            $('#error_service').fadeIn().delay(3000).fadeOut();
                        }
                    }
                });
                return false;
            }
        });
    });

    $('#delete_service_modal').on('show.bs.modal', function (e) {
        var button = e.relatedTarget;
        var value = button.value;

        $('#delete_service_btn').click(function () {
            $.ajax({
                url: 'api/clinic/profile/delete-service' + '?id=' + value,
                type: 'DELETE',
                dataType: 'json',
                contentType: "application/json",
                statusCode: {
                    200: function (data) {
                        window.location.replace("services");
                    }
                }
            });
        });
    });
});

function isValidPrice(num) {
    var numRegex = /^\d+$/;
    return numRegex.test(num);
}



package com.spduniversity.ehealth.authentication.dao.mappers;

import com.spduniversity.ehealth.model.Patient;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class PatientRowMapper implements RowMapper<Patient> {

    @Override
    public Patient mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        return Patient.newBuilder()
                .setId(resultSet.getInt("id"))
                .setFirstName(resultSet.getString("firstname"))
                .setSecondName(resultSet.getString("secondname"))
                .setUserId(resultSet.getInt("user_id"))
                .setPhone(resultSet.getString("phone"))
                .setEmail(resultSet.getString("email"))
                .build();
    }
}

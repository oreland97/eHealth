package com.spduniversity.ehealth.authentication.dao;

import com.spduniversity.ehealth.model.Clinic;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Time;
import java.time.LocalTime;

@Repository
public class ClinicDao {

    private static final String GET_CLINIC_BY_EMAIL =
            "SELECT clinics.id," +
                    " clinics.name," +
                    " clinics.email " +
                    " FROM clinics" +
                    " WHERE email = ?";

    private static final String INSERT_CLINIC =
            "INSERT INTO clinics (name, email, user_id)" +
                    "VALUES (?, ?, (SELECT id FROM users WHERE username = ?))";

    private static final String INSERT_DEFAULT_SCHEDULE =
            "INSERT INTO clinics_schedules (idclinic, timeofstart, timeofend) " +
                    "VALUES ((SELECT clinics.id FROM clinics WHERE email = ?), ?, ?)";

    private final JdbcTemplate jdbcTemplate;

    public ClinicDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Clinic getClinicByEmail(final String email) {
        try {
            return jdbcTemplate
                    .queryForObject(GET_CLINIC_BY_EMAIL, (rs, rowNum) -> Clinic.newBuilder()
                            .setId(rs.getInt("id"))
                            .setName(rs.getString("name"))
                            .setEmail(rs.getString("email"))
                            .build(), email);
        }
        catch (EmptyResultDataAccessException exception) {
            return null;
        }
    }

    public void addClinic(final String name,
                          final String email) {
        jdbcTemplate
                .update(INSERT_CLINIC, name, email, email);
        insertDefaultSchedule(email);
    }

    private void insertDefaultSchedule(final String email) {
        LocalTime time = LocalTime.of(0, 0);
        jdbcTemplate.update(INSERT_DEFAULT_SCHEDULE, email,
                Time.valueOf(time), Time.valueOf(time));
    }
}

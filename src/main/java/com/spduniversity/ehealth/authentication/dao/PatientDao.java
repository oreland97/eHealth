package com.spduniversity.ehealth.authentication.dao;

import com.spduniversity.ehealth.authentication.dao.mappers.PatientRowMapper;
import com.spduniversity.ehealth.model.Patient;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PatientDao {

    private static final String GET_PATIENT_BY_EMAIL = "SELECT * FROM patients WHERE email = ?";

    private static final String INSERT_PATIENT =
            "INSERT INTO patients (firstname, secondname, email, user_id)" +
                    "VALUES (?, ?, ?, (SELECT id FROM users WHERE username = ?))";

    private final PatientRowMapper patientRowMapper;

    private final JdbcTemplate jdbcTemplate;

    public PatientDao(PatientRowMapper patientRowMapper,
                      JdbcTemplate jdbcTemplate) {
        this.patientRowMapper = patientRowMapper;
        this.jdbcTemplate = jdbcTemplate;
    }

    public Patient getPatientByEmail(final String email) {
        try {
            return jdbcTemplate
                    .queryForObject(GET_PATIENT_BY_EMAIL, patientRowMapper, email);
        } catch (EmptyResultDataAccessException exception) {
            return null;
        }
    }

    public void addPatient(final String firstName,
                           final String secondName,
                           final String email) {
        jdbcTemplate
                .update(INSERT_PATIENT, firstName, secondName, email, email);
    }
}

package com.spduniversity.ehealth.authentication.dao.mappers;

import com.spduniversity.ehealth.model.Clinic;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ClinicRowMapper implements RowMapper<Clinic> {

    @Override
    public Clinic mapRow(ResultSet resultSet, int rowNum) throws SQLException {

        return Clinic.newBuilder()
                .setId(resultSet.getInt("id"))
                .setName(resultSet.getString("name"))
                .setEmail(resultSet.getString("email"))
                .setUrl(resultSet.getString("url"))
                .setDescription(resultSet.getString("description"))
                .setPhone(resultSet.getString("phone"))
                .build();
    }
}

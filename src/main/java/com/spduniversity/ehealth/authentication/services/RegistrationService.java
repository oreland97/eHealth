package com.spduniversity.ehealth.authentication.services;


import com.spduniversity.ehealth.authentication.dao.ClinicDao;
import com.spduniversity.ehealth.authentication.dao.PatientDao;
import com.spduniversity.ehealth.model.Clinic;
import com.spduniversity.ehealth.model.Patient;
import org.springframework.stereotype.Service;


@Service
public class RegistrationService {

    private ClinicDao clinicDao;
    private PatientDao patientDao;

    public RegistrationService(ClinicDao clinicDao, PatientDao patientDao) {
        this.clinicDao = clinicDao;
        this.patientDao = patientDao;
    }

    public boolean patientRegister(final String firstName,
                                   final String secondName,
                                   final String email,
                                   final String password) {
        Patient patient = patientDao.getPatientByEmail(email);
        if (patient == null) {
            patientDao.addPatient(firstName, secondName, email);
            return true;
        }
        return false;
    }

    public boolean clinicRegister(final String name,
                                  final String email) {
        Clinic clinic = clinicDao.getClinicByEmail(email);
        if (clinic == null) {
            clinicDao.addClinic(name, email);
            return true;
        }
        return false;
    }
}

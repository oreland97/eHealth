package com.spduniversity.ehealth.authentication.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AuthenticationController {

    @RequestMapping("/login")
    public String getLoginPage() {
        return "sign-in";
    }

    @RequestMapping("/clinic-sign-up")
    public String getClinicRegistrationPage() {
        return "clinic-sign-up";
    }

    @RequestMapping("/patient-sign-up")
    public String getPatientRegistrationPage() {
        return "patient-sign-up";
    }
}

package com.spduniversity.ehealth.authentication.controllers;

import com.spduniversity.ehealth.Role;
import com.spduniversity.ehealth.User;
import com.spduniversity.ehealth.UserService;
import com.spduniversity.ehealth.authentication.services.RegistrationService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import java.util.Optional;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@RestController
@RequestMapping("api/patient/sign_up")
public class PatientRestRegistrationController {

    private RegistrationService service;
    private UserService userService;
    private PasswordEncoder passwordEncoder;

    public PatientRestRegistrationController(RegistrationService service, UserService userService,
                                             PasswordEncoder passwordEncoder) {
        this.service = service;
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }


    @PostMapping
    public ResponseEntity patientRegistration(@RequestParam("firstName") String firstName,
                                              @RequestParam("secondName") String secondName,
                                              @RequestParam("email") String email,
                                              @RequestParam("password") String password) {
        Optional<User> user = userService.findByEmail(email);
        if (user.isPresent()) {
            return ResponseEntity.status(UNAUTHORIZED).build();
        } else {
            User newUser = new User();
            newUser.setUsername(email);
            newUser.setPassword(passwordEncoder.encode(password));
            userService.saveUser(newUser, Role.PATIENT);
            service.patientRegister(firstName, secondName, email, password);
            Authentication authentication = new UsernamePasswordAuthenticationToken(email, password);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return ResponseEntity.ok().build();
        }
    }
}

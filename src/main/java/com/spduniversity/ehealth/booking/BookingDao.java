package com.spduniversity.ehealth.booking;

import com.spduniversity.ehealth.model.*;

import java.util.List;

public interface BookingDao {

    Clinic getClinicById(int id);

    Doctor getDoctorById(int id);

    Patient getPatientByEmail(String email);

    Service getService(int serviceId, int clinicId);

    List<Doctor> getDoctorsByClinicId(int clinicId);

    List<Service> getServicesByClinicId(int clinicId);

    List<DoctorSchedule> getDoctorSchedulesByDoctorId(int doctorId);

    List<Record> getRecordsByDoctorId(int doctorId);

    int addRecord(Record record);
}

package com.spduniversity.ehealth.booking;

public enum Status {
    COMPLETE,
    INCOMPLETE,
    CANCELED
}

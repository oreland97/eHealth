package com.spduniversity.ehealth.booking;

import com.spduniversity.ehealth.model.*;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.List;

public interface BookingService {

    Clinic getClinicById(int id);

    Doctor getDoctorById(int id);

    Patient getPatientByEmail(String email);

    Service getService(int serviceId, int clinicId);

    List<Doctor> getDoctorsByClinicId(int clinicId);

    List<Service> getServicesByClinicId(int clinicId);

    List<DoctorSchedule> getDoctorSchedulesByDoctorId(int doctorId);

    List<Record> getRecordsByDoctorId(int doctorId);

    boolean book(final Patient patient,
                 final Doctor doctor,
                 final LocalTime desiredTime,
                 final DayOfWeek dayOfWeek,
                 final Service service);
}

package com.spduniversity.ehealth.booking;

import com.spduniversity.ehealth.User;
import com.spduniversity.ehealth.model.*;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

@RestController
@RequestMapping("api/booking")
public class BookingRestController {

    private final BookingService bookingService;

    public BookingRestController(final BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @PostMapping
    public ResponseEntity booking(@RequestParam final int clinicId,
                                  @RequestParam final int serviceId,
                                  @RequestParam final String time,
                                  @RequestParam final String nameOfDay,
                                  @RequestParam final int doctorId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();
        Patient patient = bookingService.getPatientByEmail(user.getUsername());
        Clinic clinic = bookingService.getClinicById(clinicId);
        clinic.setServices(bookingService.getServicesByClinicId(clinicId));
        clinic.setDoctors(bookingService.getDoctorsByClinicId(clinicId));
        Service someService = getService(serviceId, clinic);
        LocalTime localTime = LocalTime.parse(time);
        DayOfWeek dayOfWeek = getDayOfWeek(nameOfDay);
        boolean booking = bookingService.book(patient, getDoctor(doctorId, clinic),
                localTime, dayOfWeek, someService);
        if (booking) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    private Service getService(final int serviceId, final Clinic clinic) {
        for (Service service : clinic.getServices()) {
            if (service.getId() == serviceId) {
                return service;
            }
        }
        return null;
    }

    private DayOfWeek getDayOfWeek(final String nameOfDay) {
        for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
            if (nameOfDay.equalsIgnoreCase(dayOfWeek.name())) {
                return dayOfWeek;
            }
        }
        return null;
    }

    private Doctor getDoctor(final int doctorId, final Clinic clinic) {
        for (Doctor doctor : clinic.getDoctors()) {
            if (doctor.getId() == doctorId) {
                return doctor;
            }
        }
        return null;
    }

    @GetMapping("/services")
    public List<Service> getServices(@RequestParam final int clinicId) {
        return bookingService.getServicesByClinicId(clinicId);
    }

    @GetMapping("/doctors")
    public List<Doctor> getDoctors(@RequestParam final int clinicId,
                                   @RequestParam final int serviceId) {
        ArrayList<Doctor> doctors = new ArrayList<>();
        for (Doctor doctor : bookingService.getDoctorsByClinicId(clinicId)) {
            if (doctor.getSpecialization().equalsIgnoreCase(
                    bookingService.getService(serviceId, clinicId).getSpecialization())) {
                doctors.add(doctor);
            }
        }
        return doctors;
    }

    @GetMapping("/dates")
    public List<String> getDays(@RequestParam final int doctorId) {
        List<String> days = new ArrayList<>();
        int numberOfDayToday = LocalDate.now().getDayOfWeek().getValue();
        for (DayOfWeek day : getDays(bookingService.getDoctorSchedulesByDoctorId(doctorId))) {
            if (day.getValue() > numberOfDayToday || numberOfDayToday == 6 || numberOfDayToday == 7) {
                days.add(day.name());
            }
        }
        return days;
    }

    @GetMapping("/times")
    public List<String> getTimes(@RequestParam final String day,
                                 @RequestParam final int doctorId) {
        List<String> times = new ArrayList<>();
        Doctor doctor = bookingService.getDoctorById(doctorId);
        DayOfWeek dayOfWeek = parse(day);
        List<LocalTime> doctorsTimes;
        for (DoctorSchedule doctorSchedule : bookingService.getDoctorSchedulesByDoctorId(doctorId)) {
            if (doctorSchedule.getDayOfWeek().name().equalsIgnoreCase(day)) {
                doctorsTimes = getAllTimes(doctorSchedule);
                for (LocalTime doctorsTime : doctorsTimes) {
                    if (isDoctorFree(doctor, doctorsTime, dayOfWeek)) {
                        times.add(doctorsTime.toString());
                    }
                }
            }
        }
        return times;
    }

    private List<LocalTime> getAllTimes(final DoctorSchedule doctorSchedule) {
        List<LocalTime> times = new ArrayList<>();
        for (LocalTime time = doctorSchedule.getTimeOfStart(); time.isBefore(doctorSchedule.getTimeOfEnd()); time = time.plusMinutes(30)) {
            times.add(time);
        }
        return times;
    }

    private boolean isDoctorFree(final Doctor doctor,
                                 final LocalTime desiredTime,
                                 final DayOfWeek day) {
        for (DoctorSchedule doctorSchedule : bookingService.getDoctorSchedulesByDoctorId(doctor.getId())) {
            if (doctorSchedule.getDayOfWeek().equals(day) &&
                    (doctorSchedule.getTimeOfStart().isBefore(desiredTime) || doctorSchedule.getTimeOfStart() == desiredTime) &&
                    doctorSchedule.getTimeOfEnd().isAfter(desiredTime)) {
                for (Record record : bookingService.getRecordsByDoctorId(doctor.getId())) {
                    if (record.getDayOfWeek().equals(day) &&
                            record.getTime().equals(desiredTime)) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    private DayOfWeek parse(final String day) {
        for (DayOfWeek someDay : DayOfWeek.values()) {
            if (someDay.name().equalsIgnoreCase(day)) {
                return someDay;
            }
        }
        return null;
    }

    private List<DayOfWeek> getDays(final List<DoctorSchedule> schedules) {
        List<DayOfWeek> days = new ArrayList<>();
        for (DoctorSchedule schedule : schedules) {
            days.add(schedule.getDayOfWeek());
        }
        return days;
    }
}

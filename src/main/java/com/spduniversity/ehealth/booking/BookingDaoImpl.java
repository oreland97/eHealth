package com.spduniversity.ehealth.booking;

import com.spduniversity.ehealth.model.*;
import com.spduniversity.ehealth.profile.dao.mappers.DoctorRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.time.DayOfWeek;
import java.util.List;

@Repository
public class BookingDaoImpl implements BookingDao {

    private final JdbcTemplate jdbcTemplate;
    private final DoctorRowMapper doctorRowMapper;

    public BookingDaoImpl(final JdbcTemplate jdbcTemplate, final DoctorRowMapper doctorRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.doctorRowMapper = doctorRowMapper;
    }

    @Override
    public Clinic getClinicById(final int id) {
        String sql = "SELECT * FROM clinics WHERE id = ?";
        return jdbcTemplate.queryForObject(sql,
                (rs, rowNum) -> Clinic.newBuilder()
                        .setId(rs.getInt("id"))
                        .build(), id);
    }

    @Override
    public Doctor getDoctorById(final int id) {
        String sql = "SELECT" +
                " doctors.id, doctors.firstname, doctors.secondname," +
                " doctors.phone, doctors.description," +
                " specializations.name AS specialization" +
                " FROM doctors" +
                " LEFT JOIN specializations ON doctors.idspecialization = specializations.id" +
                " WHERE doctors.id = ?";
        return jdbcTemplate.queryForObject(sql, doctorRowMapper, id);
    }

    @Override
    public Patient getPatientByEmail(final String email) {
        String sql = "SELECT * FROM patients WHERE email = ?";
        return jdbcTemplate.queryForObject(sql,
                (rs, rowNum) -> Patient.newBuilder()
                        .setId(rs.getInt("id"))
                        .build(), email);
    }

    @Override
    public Service getService(final int serviceId, final int clinicId) {
        String sql = "SELECT" +
                " services.id," +
                " services.name," +
                " services_prices.price," +
                " specializations.name AS specialization," +
                " clinics.id" +
                " FROM services" +
                " INNER JOIN services_prices ON services.id = services_prices.idservice" +
                " INNER JOIN specializations ON services.idspecialization = specializations.id" +
                " INNER JOIN clinics ON services_prices.idclinic = clinics.id" +
                " WHERE services.id = ? AND clinics.id = ?";
        return jdbcTemplate.queryForObject(sql,
                (rs, rowNum) -> Service.newBuilder()
                        .setId(rs.getInt("id"))
                        .setName(rs.getString("name"))
                        .setPrice(BigDecimal.valueOf(rs.getLong("price")))
                        .setSpecialization(rs.getString("specialization"))
                        .build(), serviceId, clinicId);
    }

    @Override
    public List<Doctor> getDoctorsByClinicId(final int clinicId) {
        String sql = "SELECT" +
                " doctors.id, doctors.firstname, doctors.secondname," +
                " doctors.phone, doctors.description," +
                " specializations.name AS specialization" +
                " FROM doctors" +
                " LEFT JOIN specializations ON doctors.idspecialization = specializations.id" +
                " WHERE idclinic = ?";
        return jdbcTemplate.query(sql, doctorRowMapper, clinicId);
    }

    @Override
    public List<Service> getServicesByClinicId(final int clinicId) {
        String sql = "SELECT services.id," +
                " services.name," +
                " services_prices.price FROM services " +
                "INNER JOIN services_prices ON" +
                " services.id = services_prices.idservice "
                + "WHERE idclinic = ? ORDER BY name";
        return jdbcTemplate.query(sql,
                (rs, rowNum) -> Service.newBuilder()
                        .setId(rs.getInt("id"))
                        .setName(rs.getString("name"))
                        .setPrice(BigDecimal.valueOf(rs.getLong("price")))
                        .build(), clinicId);
    }

    @Override
    public List<DoctorSchedule> getDoctorSchedulesByDoctorId(final int doctorId) {
        String sql = "SELECT * FROM doctors_schedules WHERE iddoctor = ?";
        return jdbcTemplate.query(sql,
                (rs, rowNum) -> DoctorSchedule.newBuilder()
                        .setDayOfWeek(parse(rs.getString("dayofweek")))
                        .setTimeOfStart(rs.getTime("timeofstart").toLocalTime())
                        .setTimeOfEnd(rs.getTime("timeofend").toLocalTime())
                        .build(), doctorId);
    }

    private DayOfWeek parse(final String nameOfDay) {
        for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
            if (nameOfDay.equalsIgnoreCase(dayOfWeek.name())) {
                return dayOfWeek;
            }
        }
        return null;
    }

    @Override
    public List<Record> getRecordsByDoctorId(final int doctorId) {
        String sql = "SELECT * FROM records WHERE iddoctor = ?";
        return jdbcTemplate.query(sql,
                (rs, rowNum) -> {
                    Record record = new Record();
                    record.setDayOfWeek(parse(rs.getString("dayofweek")));
                    record.setTime(rs.getTime("time").toLocalTime());
                    return record;
                }, doctorId);
    }

    @Override
    public int addRecord(final Record record) {
        String sql = "INSERT INTO records (time, dayofweek, date, idPatient, idDoctor, idservice, status)" +
                " VALUES (?,CAST(? AS DAY),?,?,?,?,CAST(? AS STATUS))";
        return jdbcTemplate.update(sql,
                Time.valueOf(record.getTime()),
                record.getDayOfWeek().name().toLowerCase(),
                Date.valueOf(record.getDate()),
                record.getPatient().getId(),
                record.getDoctor().getId(),
                record.getService().getId(),
                Status.INCOMPLETE.name().toLowerCase());
    }
}


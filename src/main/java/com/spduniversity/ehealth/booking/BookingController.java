package com.spduniversity.ehealth.booking;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BookingController {

    @RequestMapping("/booking")
    public String getBookingPage() {
        return "make-appointment";
    }
}

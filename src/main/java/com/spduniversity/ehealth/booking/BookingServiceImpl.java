package com.spduniversity.ehealth.booking;

import com.spduniversity.ehealth.model.*;
import org.threeten.extra.YearWeek;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@org.springframework.stereotype.Service
public class BookingServiceImpl implements BookingService {

    private final BookingDao bookingDao;

    public BookingServiceImpl(final BookingDao bookingDao) {
        this.bookingDao = bookingDao;
    }

    @Override
    public boolean book(final Patient patient,
                        final Doctor doctor,
                        final LocalTime desiredTime,
                        final DayOfWeek dayOfWeek,
                        final Service service) {
        Record record = new Record(parseToLocalDate(dayOfWeek), desiredTime, patient, doctor, dayOfWeek, service);
        return bookingDao.addRecord(record) > 0;
    }

    private LocalDate parseToLocalDate(final DayOfWeek dayOfWeek) {
        LocalDate today = LocalDate.now();
        YearWeek yearWeek = YearWeek.from(today);
        if (today.getDayOfWeek().getValue() == 6 || today.getDayOfWeek().getValue() == 7) {
            yearWeek = yearWeek.plusWeeks(1);
        }
        return yearWeek.atDay(dayOfWeek);
    }

    @Override
    public Clinic getClinicById(final int id) {
        return bookingDao.getClinicById(id);
    }

    @Override
    public Doctor getDoctorById(final int id) {
        return bookingDao.getDoctorById(id);
    }

    @Override
    public Patient getPatientByEmail(final String email) {
        return bookingDao.getPatientByEmail(email);
    }

    @Override
    public Service getService(final int serviceId, final int clinicId) {
        return bookingDao.getService(serviceId, clinicId);
    }

    @Override
    public List<Doctor> getDoctorsByClinicId(final int clinicId) {
        return bookingDao.getDoctorsByClinicId(clinicId);
    }

    @Override
    public List<Service> getServicesByClinicId(final int clinicId) {
        return bookingDao.getServicesByClinicId(clinicId);
    }

    @Override
    public List<DoctorSchedule> getDoctorSchedulesByDoctorId(final int doctorId) {
        return bookingDao.getDoctorSchedulesByDoctorId(doctorId);
    }

    @Override
    public List<Record> getRecordsByDoctorId(final int doctorId) {
        return bookingDao.getRecordsByDoctorId(doctorId);
    }
}

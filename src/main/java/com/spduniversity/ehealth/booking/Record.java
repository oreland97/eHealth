package com.spduniversity.ehealth.booking;

import com.spduniversity.ehealth.model.Doctor;
import com.spduniversity.ehealth.model.Patient;
import com.spduniversity.ehealth.model.Service;
import com.spduniversity.ehealth.records.Feedback;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;

public class Record {

    private int id;
    private LocalDate date;
    private LocalTime time;
    private DayOfWeek dayOfWeek;
    private Patient patient;
    private Doctor doctor;
    private Service service;
    private Status status;
    private Feedback feedback;

    public Record(LocalDate date, LocalTime time, Patient patient, Doctor doctor, DayOfWeek dayOfWeek, Service service) {
        this.date = date;
        this.time = time;
        this.patient = patient;
        this.doctor = doctor;
        this.dayOfWeek = dayOfWeek;
        this.service = service;
    }

    public Record() {
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Feedback getFeedback() {
        return feedback;
    }

    public void setFeedback(Feedback feedback) {
        this.feedback = feedback;
    }
}

package com.spduniversity.ehealth.model;

import java.time.DayOfWeek;
import java.time.LocalTime;

public class DoctorSchedule {

    private DayOfWeek dayOfWeek;
    private LocalTime timeOfStart;
    private LocalTime timeOfEnd;

    private DoctorSchedule() {
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public LocalTime getTimeOfStart() {
        return timeOfStart;
    }

    public LocalTime getTimeOfEnd() {
        return timeOfEnd;
    }

    public static Builder newBuilder() {
        return new DoctorSchedule()
                .new Builder();
    }

    @Override
    public String toString() {
        return "DoctorSchedule{" +
                "dayOfWeek=" + dayOfWeek +
                ", timeOfStart=" + timeOfStart +
                ", timeOfEnd=" + timeOfEnd +
                '}';
    }

    public class Builder {

        private Builder() {
        }

        public Builder setDayOfWeek(final DayOfWeek dayOfWeek) {
            DoctorSchedule.this.dayOfWeek = dayOfWeek;
            return this;
        }

        public Builder setTimeOfStart(final LocalTime timeOfStart) {
            DoctorSchedule.this.timeOfStart = timeOfStart;
            return this;
        }

        public Builder setTimeOfEnd(final LocalTime timeOfEnd) {
            DoctorSchedule.this.timeOfEnd = timeOfEnd;
            return this;
        }

        public DoctorSchedule build() {
            return DoctorSchedule.this;
        }
    }
}

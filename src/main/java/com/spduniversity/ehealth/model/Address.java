package com.spduniversity.ehealth.model;


public class Address {

    private int id;
    private String region;
    private String city;
    private String location;

    private Address() {
    }

    public int getId() {
        return id;
    }

    public String getRegion() {
        return region;
    }

    public String getCity() {
        return city;
    }

    public String getLocation() {
        return location;
    }

    public static Builder newBuilder() {
        return new Address()
                .new Builder();
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", region='" + region + '\'' +
                ", city='" + city + '\'' +
                ", location='" + location + '\'' +
                '}';
    }

    public class Builder {

        private Builder() {
        }

        public Builder setId(final int id) {
            Address.this.id = id;
            return this;
        }

        public Builder setRegion(final String region) {
            Address.this.region = region;
            return this;
        }

        public Builder setCity(final String city) {
            Address.this.city = city;
            return this;
        }

        public Builder setLocation(final String location) {
            Address.this.location = location;
            return this;
        }

        public Address build() {
            return Address.this;
        }
    }
}

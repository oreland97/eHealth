package com.spduniversity.ehealth.model;


import java.math.BigDecimal;

public class Service {

    private int id;
    private String name;
    private BigDecimal price;
    private String specialization;

    public Service() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getSpecialization() {
        return specialization;
    }

    public static Builder newBuilder() {
        return new Service()
                .new Builder();
    }

    @Override
    public String toString() {
        return "Service{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }

    public class Builder {

        private Builder() {
        }

        public Builder setId(final int id) {
            Service.this.id = id;
            return this;
        }

        public Builder setName(final String name) {
            Service.this.name = name;
            return this;
        }

        public Builder setPrice(final BigDecimal price) {
            Service.this.price = price;
            return this;
        }

        public Builder setSpecialization(final String specialization) {
            Service.this.specialization = specialization;
            return this;
        }

        public Service build() {
            return Service.this;
        }

    }

}

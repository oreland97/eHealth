package com.spduniversity.ehealth.model;


import com.spduniversity.ehealth.booking.Record;

import java.util.List;

public class Patient {

    private int id;
    private int userId;
    private String firstName;
    private String secondName;
    private String phone;
    private String email;
    private List<Record> records;

    public Patient() {
    }

    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public List<Record> getRecords() {
        return records;
    }

    public static Builder newBuilder() {
        return new Patient()
                .new Builder();
    }

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", records=" + records +
                '}';
    }

    public class Builder {

        private Builder() {
        }

        public Builder setId(final int id) {
            Patient.this.id = id;
            return this;
        }

        public Builder setUserId(final int userId) {
            Patient.this.userId = userId;
            return this;
        }

        public Builder setFirstName(final String firstName) {
            Patient.this.firstName = firstName;
            return this;
        }

        public Builder setSecondName(final String secondName) {
            Patient.this.secondName = secondName;
            return this;
        }

        public Builder setPhone(final String phone) {
            Patient.this.phone = phone;
            return this;
        }

        public Builder setEmail(final String email) {
            Patient.this.email = email;
            return this;
        }

        public Builder setRecords(final List<Record> records) {
            Patient.this.records = records;
            return this;
        }

        public Patient build() {
            return Patient.this;
        }
    }
}

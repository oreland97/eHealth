package com.spduniversity.ehealth.model;

import java.util.List;

public class Doctor {

    private int id;
    private String firstName;
    private String secondName;
    private String specialization;
    private List<DoctorSchedule> schedules;
    private String description;
    private String phone;
    private String status;

    public Doctor() { }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getSpecialization() {
        return specialization;
    }

    public List<DoctorSchedule> getSchedules() {
        return schedules;
    }

    public String getDescription() {
        return description;
    }

    public String getPhone() {
        return phone;
    }

    public String getStatus() {
        return status;
    }

    public static Builder newBuilder() {
        return new Doctor()
                .new Builder();
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", specialization='" + specialization + '\'' +
                ", schedules=" + schedules +
                ", description='" + description + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }

    public class Builder {

        private Builder() {
        }

        public Builder setId(final int id) {
            Doctor.this.id = id;
            return this;
        }

        public Builder setFirstName(final String firstName) {
            Doctor.this.firstName = firstName;
            return this;
        }

        public Builder setSecondName(final String secondName) {
            Doctor.this.secondName = secondName;
            return this;
        }

        public Builder setSpecialization(final String specialization) {
            Doctor.this.specialization = specialization;
            return this;
        }

        public Builder setSchedules(final List<DoctorSchedule> schedules) {
            Doctor.this.schedules = schedules;
            return this;
        }

        public Builder setDescription(final String description) {
            Doctor.this.description = description;
            return this;
        }

        public Builder setPhone(final String phone) {
            Doctor.this.phone = phone;
            return this;
        }

        public Builder setStatus(final String status) {
            Doctor.this.status = status;
            return this;
        }

        public Doctor build() {
            return Doctor.this;
        }
    }
}

package com.spduniversity.ehealth.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalTime;

public class ClinicSchedule {

    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private LocalTime timeOfStart;

    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private LocalTime timeOfEnd;

    private ClinicSchedule() {
    }

    public LocalTime getTimeOfStart() {
        return timeOfStart;
    }

    public LocalTime getTimeOfEnd() {
        return timeOfEnd;
    }

    public static Builder newBuilder() {
        return new ClinicSchedule()
                .new Builder();
    }

    @Override
    public String toString() {
        return "ClinicSchedule{" +
                "timeOfStart=" + timeOfStart +
                ", timeOfEnd=" + timeOfEnd +
                '}';
    }

    public class Builder {

        private Builder() {

        }

        public Builder setTimeOfStart(final LocalTime timeOfStart) {
            ClinicSchedule.this.timeOfStart = timeOfStart;
            return this;
        }

        public Builder setTimeOfEnd(final LocalTime timeOfEnd) {
            ClinicSchedule.this.timeOfEnd = timeOfEnd;
            return this;
        }

        public ClinicSchedule build() {
            return ClinicSchedule.this;
        }
    }
}

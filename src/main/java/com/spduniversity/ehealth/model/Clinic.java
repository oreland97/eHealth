package com.spduniversity.ehealth.model;

import com.spduniversity.ehealth.booking.Record;
import com.spduniversity.ehealth.records.Feedback;

import java.util.List;

public class Clinic {

    private int id;
    private int userId;
    private String name;
    private List<String> specializations;
    private Address address;
    private List<Doctor> doctors;
    private List<Service> services;
    private String description;
    private ClinicSchedule schedule;
    private String url;
    private String phone;
    private List<Record> records;
    private List<Feedback> feedbacks;
    private String email;

    private Clinic() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<String> getSpecializations() {
        return specializations;
    }

    public Address getAddress() {
        return address;
    }

    public int getUserId() {
        return userId;
    }

    public List<Doctor> getDoctors() {
        return doctors;
    }

    public List<Service> getServices() {
        return services;
    }

    public String getDescription() {
        return description;
    }

    public ClinicSchedule getSchedule() {
        return schedule;
    }

    public String getUrl() {
        return url;
    }

    public String getPhone() {
        return phone;
    }

    public List<Record> getRecords() {
        return records;
    }

    public List<Feedback> getFeedbacks() {
        return feedbacks;
    }

    public String getEmail() {
        return email;
    }

    public void setDoctors(List<Doctor> doctors) {
        this.doctors = doctors;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    @Override
    public String toString() {
        return "Clinic{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", specializations=" + specializations +
                ", address=" + address +
                ", doctors=" + doctors +
                ", services=" + services +
                ", description='" + description + '\'' +
                ", schedule=" + schedule +
                ", url=" + url +
                ", phone='" + phone + '\'' +
                ", records=" + records +
                ", listOfFeedback=" + feedbacks +
                ", email='" + email + '\'' +
                '}';
    }

    public static Builder newBuilder() {
        return new Clinic()
                .new Builder();
    }

    public class Builder {

        private Builder() {
        }

        public Builder setId(final int id) {
            Clinic.this.id = id;
            return this;
        }

        public Builder setName(final String name) {
            Clinic.this.name = name;
            return this;
        }

        public Builder setUserId(final int userId) {
            Clinic.this.userId = userId;
            return this;
        }

        public Builder setSpecializations(final List<String> specializations) {
            Clinic.this.specializations = specializations;
            return this;
        }

        public Builder setAddress(final Address address) {
            Clinic.this.address = address;
            return this;
        }

        public Builder setDoctors(final List<Doctor> doctors) {
            Clinic.this.doctors = doctors;
            return this;
        }

        public Builder setServices(final List<Service> services) {
            Clinic.this.services = services;
            return this;
        }

        public Builder setDescription(final String description) {
            Clinic.this.description = description;
            return this;
        }

        public Builder setSchedule(final ClinicSchedule schedule) {
            Clinic.this.schedule = schedule;
            return this;
        }

        public Builder setUrl(final String url) {
            Clinic.this.url = url;
            return this;
        }

        public Builder setPhone(final String phone) {
            Clinic.this.phone = phone;
            return this;
        }

        public Builder setRecords(final List<Record> records) {
            Clinic.this.records = records;
            return this;
        }

        public Builder setFeedaback(final List<Feedback> feedbacks){
            Clinic.this.feedbacks = feedbacks;
            return this;
        }

        public Builder setEmail(final String email) {
            Clinic.this.email = email;
            return this;
        }

        public Clinic build() {
            return Clinic.this;
        }
    }
}

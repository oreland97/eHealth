package com.spduniversity.ehealth;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.Optional;

@Repository
public class UserDao {

    private final JdbcTemplate jdbcTemplate;

    public UserDao(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Optional<User> findByUsername(final String username) {
        return jdbcTemplate.queryForObject("SELECT * FROM users WHERE username = ?",
                (rs, rowNum) -> {
                    User user = new User(
                            rs.getInt("id"),
                            Arrays.asList(parseRole(rs.getString("role"))),
                            rs.getString("password"),
                            rs.getString("username"),
                            rs.getBoolean("accountnonexpired"),
                            rs.getBoolean("accountnonlocked"),
                            rs.getBoolean("credentialsnonexpired"),
                            rs.getBoolean("enabled"));
                    return Optional.of(user);
                }, username);
    }

    private Role parseRole(final String role) {
        for (Role someRole : Role.values()) {
            if (someRole.name().equalsIgnoreCase(role)) {
                return someRole;
            }
        }
        return null;
    }

    public void saveUser(final User user, final Role role) {
        String sql = "INSERT INTO users (username, password, accountnonexpired," +
                " accountnonlocked, credentialsnonexpired, enabled, role) " +
                "VALUES (?, ?, ?, ?, ?, ?, CAST(? AS ROLE))";
        jdbcTemplate.update(sql, user.getUsername(), user.getPassword(), true,
                true, true, true, convertRole(role));
    }

    private String convertRole(final Role role) {
        if (role.name().equals("CLINIC")) {
            return "Clinic";
        }
        return "Patient";
    }

    public Optional<User> findByEmail(final String email) {
        String sql = "SELECT * FROM users WHERE username = ?";
        try {
            return jdbcTemplate.queryForObject(sql, (rs, rowNum) -> {
                User user = new User();
                user.setUsername(rs.getString("username"));
                return Optional.of(user);
            }, email);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }
}

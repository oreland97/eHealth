package com.spduniversity.ehealth;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    CLINIC, PATIENT;

    @Override
    public String getAuthority() {
        return name();
    }
}

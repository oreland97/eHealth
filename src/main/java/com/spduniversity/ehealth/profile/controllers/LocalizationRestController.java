package com.spduniversity.ehealth.profile.controllers;

import com.spduniversity.ehealth.profile.services.LocalizationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/localization")
public class LocalizationRestController {

    private final LocalizationService localizationService;

    public LocalizationRestController(final LocalizationService localizationService) {
        this.localizationService = localizationService;
    }

    @GetMapping("/regions")
    public ResponseEntity getAllRegions() {
        return ResponseEntity.ok(localizationService.getListOfRegions());
    }

    @GetMapping("/cities")
    public ResponseEntity getCityByRegion(@RequestParam(required = false) final String selectedRegion) {
        return ResponseEntity.ok(localizationService.getListOfCitiesByRegion(selectedRegion));
    }
}

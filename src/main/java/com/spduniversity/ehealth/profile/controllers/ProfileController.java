package com.spduniversity.ehealth.profile.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ProfileController {

    @RequestMapping("/clinic-edit")
    public String getClinicProfilePage() {
        return "clinic-edit-profile";
    }

    @RequestMapping("/patient-edit")
    public String getPatientProfilePage() {
        return "patient-edit-profile";
    }

    @RequestMapping("/doctors-table")
    public String getDoctorsTablePage() {
        return "doctors-table";
    }

    @RequestMapping("/services")
    public String getServicesPage() {
        return "clinic-services";
    }
}

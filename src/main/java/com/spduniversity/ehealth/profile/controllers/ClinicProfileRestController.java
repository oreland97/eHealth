package com.spduniversity.ehealth.profile.controllers;

import com.spduniversity.ehealth.User;
import com.spduniversity.ehealth.model.Clinic;
import com.spduniversity.ehealth.model.Doctor;
import com.spduniversity.ehealth.model.Service;
import com.spduniversity.ehealth.profile.dto.DoctorDto;
import com.spduniversity.ehealth.profile.services.ClinicProfileService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/clinic/profile")
public class ClinicProfileRestController {

    private final ClinicProfileService service;

    public ClinicProfileRestController(final ClinicProfileService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<Clinic> getClinicProfile(final Principal principal) {
        String email = principal.getName();
        if (email != null) {
            Clinic clinic = service.getClinicByEmail(email);
            return ResponseEntity.ok(clinic);
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping
    public ResponseEntity updateClinicProfile(@RequestBody final Clinic clinic) {
        service.setClinicToDao(clinic);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User userDetails = (User) authentication.getPrincipal();
        userDetails.setUsername(clinic.getEmail());
        return ResponseEntity.ok().build();
    }

    @GetMapping("/doctors")
    public ResponseEntity getDoctorsByClinicEmail(final Principal principal) {
        String email = principal.getName();
        List<Doctor> doctors = service.getDoctorsByClinicEmail(email);
        if (doctors != null) {
            return ResponseEntity.ok(doctors);
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/specializations")
    public ResponseEntity getAllSpecializations() {
        return ResponseEntity.ok(service.getAllSpecializations());
    }

    @PostMapping("/add-doctor")
    public ResponseEntity addNewDoctorToClinic(@RequestBody final Doctor doctor,
                                               final Principal principal) {
        String clinicEmail = principal.getName();
        service.addNewDoctorToClinic(clinicEmail, doctor);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/get-doctor")
    public ResponseEntity<Doctor> getDoctorById(@RequestParam final int id) {
        return ResponseEntity.ok(service.getDoctorById(id));
    }

    @PostMapping("/update-doctor")
    public ResponseEntity updateDoctorById(@RequestBody final Doctor doctor) {
        service.updateDoctorById(doctor);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/update-doctor-schedule")
    public ResponseEntity updateDoctorSchedule(@RequestBody final DoctorDto doctorDto) {
        service.updateDoctorSchedule(doctorDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/delete-doctor")
    public ResponseEntity deleteDoctorById(@RequestParam("id") final int id) {
        service.deleteDoctorById(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/services")
    public ResponseEntity getServicesByClinic(final Principal principal) {
        String clinicEmail = principal.getName();
        return ResponseEntity.ok(service.getServicesByClinic(clinicEmail));
    }

    @PostMapping("/add-service")
    public ResponseEntity addServiceToClinic(@RequestBody final Service serviceFromResponse,
                                             final Principal principal) {
        String clinicEmail = principal.getName();
        Optional<Service> serviceByClinicId = service.getServiceByClinicId(clinicEmail, serviceFromResponse);
        if (serviceByClinicId.isPresent()) {
            return ResponseEntity.badRequest().build();
        } else {
            service.addServiceToClinic(serviceFromResponse, clinicEmail);
            return ResponseEntity.ok().build();
        }
    }

    @GetMapping("/all-services")
    public ResponseEntity getAllServices(final Principal principal) {
        String clinicEmail = principal.getName();
        return ResponseEntity.ok(service.getAllServices(clinicEmail));
    }

    @DeleteMapping("/delete-service")
    public ResponseEntity deleteService(@RequestParam final int id,
                                        final Principal principal) {
        String clinicEmail = principal.getName();
        service.deleteService(id, clinicEmail);
        return ResponseEntity.ok().build();
    }
}

package com.spduniversity.ehealth.profile.controllers;

import com.spduniversity.ehealth.User;
import com.spduniversity.ehealth.model.Patient;
import com.spduniversity.ehealth.profile.services.PatientProfileService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("api/patient/profile")
public class PatientProfileRestController {

    private final PatientProfileService service;

    public PatientProfileRestController(final PatientProfileService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<Patient> getPatientProfile(final Principal principal) {
        String email = principal.getName();
        if (email != null) {
            Patient patient = service.getPatientByEmail(email);
            return ResponseEntity.ok(patient);
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping
    public ResponseEntity updatePatientProfile(@RequestBody final Patient patient) {
        service.setPatientToDao(patient);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User userDetails = (User) authentication.getPrincipal();
        userDetails.setUsername(patient.getEmail());
        return ResponseEntity.ok().build();
    }
}

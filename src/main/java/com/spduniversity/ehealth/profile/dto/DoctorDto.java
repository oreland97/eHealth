package com.spduniversity.ehealth.profile.dto;

import com.spduniversity.ehealth.model.DoctorSchedule;

import java.util.List;

public class DoctorDto {

    private int id;
    private List<DoctorSchedule> schedules;

    private DoctorDto() {
    }

    public int getId() {
        return id;
    }

    public List<DoctorSchedule> getSchedules() {
        return schedules;
    }

    public static Builder newBuilder() {
        return new DoctorDto()
                .new Builder();
    }

    @Override
    public String toString() {
        return "DoctorDto{" +
                "id=" + id +
                ", schedules=" + schedules +
                '}';
    }

    public class Builder {

        private Builder() {
        }

        public Builder setId(final int id) {
            DoctorDto.this.id = id;
            return this;
        }

        public Builder setSchedules(final List<DoctorSchedule> schedules) {
            DoctorDto.this.schedules = schedules;
            return this;
        }

        public DoctorDto build() {
            return DoctorDto.this;
        }
    }
}

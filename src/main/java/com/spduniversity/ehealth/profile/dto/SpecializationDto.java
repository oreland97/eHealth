package com.spduniversity.ehealth.profile.dto;

public class SpecializationDto {

    private int id;
    private String name;

    private SpecializationDto() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static Builder newBuilder() {
        return new SpecializationDto()
                .new Builder();
    }

    public class Builder {

        private Builder() {
        }

        public Builder setId(final int id) {
            SpecializationDto.this.id = id;
            return this;
        }

        public Builder setName(final String name) {
            SpecializationDto.this.name = name;
            return this;
        }

        public SpecializationDto build() {
            return SpecializationDto.this;
        }
    }
}

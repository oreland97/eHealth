package com.spduniversity.ehealth.profile.viewprofile;

import com.spduniversity.ehealth.model.*;
import com.spduniversity.ehealth.profile.dao.mappers.DoctorRowMapper;
import com.spduniversity.ehealth.records.Feedback;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public class ViewProfileDaoImpl implements ViewProfileDao {

    private static final String CLINIC_INFO_BY_ID_SQL = "SELECT" +
            " clinics.id AS id," +
            " clinics.name AS name," +
            " clinics.url AS url," +
            " clinics.description AS description," +
            " clinics.phone AS phone," +
            " regions.name AS region," +
            " cities.name AS city," +
            " addresses.location AS location," +
            " clinics_schedules.timeofstart AS timeOfStart," +
            " clinics_schedules.timeofend AS timeOfEnd" +
            " FROM addresses" +
            " LEFT JOIN clinics ON addresses.idclinic = clinics.id" +
            " LEFT JOIN cities ON addresses.idcity = cities.id" +
            " LEFT JOIN regions ON cities.idregion = regions.id" +
            " LEFT JOIN clinics_schedules ON clinics.id = clinics_schedules.idclinic" +
            " WHERE clinics.id = ?";

    private static final String SPECIALIZATIONS_BY_CLINIC_ID_SQL = "SELECT" +
            " specializations.name AS specialization" +
            " FROM clinics" +
            " LEFT JOIN clinics_specializations ON" +
            " clinics.id = clinics_specializations.idclinic" +
            " LEFT JOIN specializations ON" +
            " clinics_specializations.idspecialization = specializations.id" +
            " WHERE clinics.id = ?";

    private static final String DOCTORS_BY_CLINIC_ID_SQL = "SELECT" +
            " doctors.id, doctors.firstname, doctors.secondname," +
            " doctors.phone, doctors.description," +
            " specializations.name AS specialization" +
            " FROM doctors" +
            " LEFT JOIN specializations ON doctors.idspecialization = specializations.id" +
            " WHERE idclinic = ?";

    private static final String SERVICES_BY_CLINIC_ID_SQL = "SELECT" +
            " services.name AS service," +
            " services_prices.price AS price" +
            " FROM clinics" +
            " LEFT JOIN services_prices ON clinics.id = services_prices.idclinic" +
            " LEFT JOIN services ON services_prices.idservice = services.id" +
            " WHERE clinics.id = ?";

    private static final String FEEDBACKS_BY_CLINIC_ID_SQL = "SELECT" +
            " clinics.id AS id," +
            " feedbacks.datetime AS dateTime," +
            " feedbacks.rating   AS rating," +
            " feedbacks.description AS description," +
            " patients.firstname AS firstName," +
            " patients.secondname AS secondName" +
            " FROM feedbacks" +
            " LEFT JOIN records ON feedbacks.idrecord = records.id" +
            " LEFT JOIN patients ON records.idpatient = patients.id" +
            " LEFT JOIN doctors ON records.iddoctor = doctors.id" +
            " LEFT JOIN clinics ON doctors.idclinic = clinics.id" +
            " WHERE clinics.id = ?";

    private final JdbcTemplate jdbcTemplate;
    private final DoctorRowMapper doctorRowMapper;

    public ViewProfileDaoImpl(final JdbcTemplate jdbcTemplate, final DoctorRowMapper doctorRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.doctorRowMapper = doctorRowMapper;
    }

    @Override
    public Clinic getClinicById(final int id) {
        return jdbcTemplate.queryForObject(CLINIC_INFO_BY_ID_SQL,
                (rs, rowNum) -> {
                    Address address = Address.newBuilder()
                            .setRegion(rs.getString("region"))
                            .setCity(rs.getString("city"))
                            .setLocation(rs.getString("location"))
                            .build();
                    ClinicSchedule schedule = ClinicSchedule.newBuilder()
                            .setTimeOfStart(rs.getTime("timeOfStart").toLocalTime())
                            .setTimeOfEnd(rs.getTime("timeOfEnd").toLocalTime())
                            .build();
                    return Clinic.newBuilder()
                            .setId(rs.getInt("id"))
                            .setName(rs.getString("name"))
                            .setUrl(rs.getString("url"))
                            .setDescription(rs.getString("description"))
                            .setPhone(rs.getString("phone"))
                            .setAddress(address)
                            .setSchedule(schedule)
                            .setDoctors(getDoctorsByClinicId(rs.getInt("id")))
                            .setSpecializations(getSpecializationsByClinicId(rs.getInt("id")))
                            .setFeedaback(getFeedbacksByClinicId(rs.getInt("id")))
                            .setServices(getServicesByClinicId(rs.getInt("id")))
                            .build();
                }, id);
    }

    private List<Doctor> getDoctorsByClinicId(final int clinicId) {
        return jdbcTemplate.query(DOCTORS_BY_CLINIC_ID_SQL,
                doctorRowMapper, clinicId);
    }

    private List<String> getSpecializationsByClinicId(final int clinicId) {
        return jdbcTemplate.query(SPECIALIZATIONS_BY_CLINIC_ID_SQL,
                (rs, rowNum) -> rs.getString("specialization"), clinicId);
    }

    private List<Service> getServicesByClinicId(final int clinicId) {
        return jdbcTemplate.query(SERVICES_BY_CLINIC_ID_SQL,
                (rs, rowNum) -> Service.newBuilder()
                        .setName(rs.getString("service"))
                        .setPrice(BigDecimal.valueOf(rs.getLong("price")))
                        .build(), clinicId);
    }

    private List<Feedback> getFeedbacksByClinicId(final int clinicId) {
        return jdbcTemplate.query(FEEDBACKS_BY_CLINIC_ID_SQL,
                (rs, rowNum) -> {
                    Feedback feedback = new Feedback();
                    feedback.setDateTime(rs.getTimestamp("dateTime").toLocalDateTime());
                    feedback.setDescription(rs.getString("description"));
                    feedback.setRating(rs.getInt("rating"));
                    Patient patient = Patient.newBuilder()
                            .setFirstName(rs.getString("firstName"))
                            .setSecondName(rs.getString("secondName"))
                            .build();
                    feedback.setPatient(patient);
                    return feedback;
                }, clinicId);
    }
}

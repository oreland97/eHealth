package com.spduniversity.ehealth.profile.viewprofile;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewProfileControllerController {

    @RequestMapping("/clinic-info")
    public String getClinicInfo() {
        return "clinic-info";
    }
}

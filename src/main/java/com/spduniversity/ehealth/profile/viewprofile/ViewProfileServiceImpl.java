package com.spduniversity.ehealth.profile.viewprofile;

import com.spduniversity.ehealth.model.Clinic;
import org.springframework.stereotype.Service;

@Service
public class ViewProfileServiceImpl implements ViewProfileService {

    private final ViewProfileDao viewProfileDao;

    public ViewProfileServiceImpl(ViewProfileDao viewProfileDao) {
        this.viewProfileDao = viewProfileDao;
    }

    @Override
    public Clinic getClinicById(final int id) {
        return viewProfileDao.getClinicById(id);
    }
}

package com.spduniversity.ehealth.profile.viewprofile;

import com.spduniversity.ehealth.model.Clinic;

public interface ViewProfileDao {

    Clinic getClinicById(int id);
}

package com.spduniversity.ehealth.profile.viewprofile;

import com.spduniversity.ehealth.model.Clinic;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/clinic-info")
public class ViewClinicProfileRestController {

    private final ViewProfileService viewProfileService;

    public ViewClinicProfileRestController(final ViewProfileService viewProfileService) {
        this.viewProfileService = viewProfileService;
    }

    @GetMapping
    public ResponseEntity<Clinic> showInfo(@RequestParam final int clinicId) {
        Clinic clinic = viewProfileService.getClinicById(clinicId);
        return ResponseEntity.ok(clinic);
    }
}


package com.spduniversity.ehealth.profile.viewprofile;

import com.spduniversity.ehealth.model.Clinic;

public interface ViewProfileService {

    Clinic getClinicById(int id);
}

package com.spduniversity.ehealth.profile.dao;

import com.spduniversity.ehealth.authentication.dao.mappers.PatientRowMapper;
import com.spduniversity.ehealth.model.*;
import com.spduniversity.ehealth.profile.dao.mappers.DoctorRowMapper;
import com.spduniversity.ehealth.profile.dto.SpecializationDto;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class ProfileDao {

    private static final String GET_CLINIC_BY_EMAIL =
            "SELECT clinics.id," +
                    " clinics.name," +
                    " clinics.phone, clinics.url," +
                    " clinics.email, " +
                    " clinics.description, clinics.user_id," +
                    " clinics_schedules.timeofend," +
                    "clinics_schedules.timeofstart" +
                    " FROM clinics " +
                    " LEFT JOIN" +
                    " clinics_schedules ON clinics.id = clinics_schedules.idclinic" +
                    " WHERE email = ?";

    private static final String GET_PATIENT_BY_EMAIL = "SELECT * FROM patients WHERE email = ?";

    private static final String UPDATE_PATIENT =
            "UPDATE patients SET (firstname, secondname, phone, email)" +
                    "= (?, ?, ?, ?) WHERE id = ?";


    private static final String UPDATE_CLINIC =
            "UPDATE clinics SET (name, email, phone, url, description)" +
                    "= (?, ?, ?, ?, ?) WHERE id = ?";

    private static final String GET_ALL_REGIONS = "SELECT regions.name FROM regions ORDER BY regions.name";

    private static final String GET_CITIES_BY_REGION = "SELECT cities.name AS city " +
            " FROM cities, regions " +
            " WHERE cities.idregion = regions.id " +
            "AND regions.name = ? " +
            "ORDER BY cities.name";

    private static final String GET_ADDRESS_BY_CLINIC_ID = "SELECT" +
            "  addresses.idclinic AS clinic," +
            "  cities.name        AS city," +
            "  regions.name       AS region," +
            "  addresses.location AS location " +
            "FROM addresses" +
            "  LEFT JOIN cities ON addresses.idcity = cities.id" +
            "  LEFT JOIN regions ON cities.idregion = regions.id " +
            "WHERE addresses.idclinic = ?";

    private static final String UPDATE_ADDRESS =
            "UPDATE addresses SET (idcity, location, idclinic) " +
                    "= ((SELECT id FROM cities WHERE name = ?)," +
                    " ?, ?) WHERE idclinic = ?";

    private final static String INSERT_ADDRESS =
            "INSERT INTO addresses (idcity, location, idclinic) " +
                    "VALUES ((SELECT id FROM cities WHERE name = ?), " +
                    " ?, ?)";

    private static final String GET_DOCTORS_BY_CLINIC_ID =
            "SELECT firstname, secondname, phone, description, doctors.id, " +
                    " specializations.name AS specialization, doctors.status FROM doctors " +
                    "LEFT JOIN specializations ON doctors.idspecialization = specializations.id " +
                    "WHERE idclinic = (SELECT id FROM clinics WHERE email = ?) AND " +
                    "doctors.status = 'active'";

    private static final String INSERT_NEW_DOCTOR = "INSERT INTO doctors " +
            "(firstname, secondname, phone, description, idclinic, idspecialization, status) " +
            "VALUES (?, ?, ?, ? ,(SELECT id FROM clinics WHERE email = ?), ?, 'active') ";

    private static final String GET_DOCTOR_BY_ID =
            "SELECT firstname, secondname, phone, description, doctors.id, " +
                    "specializations.name AS specialization FROM doctors " +
                    "LEFT JOIN specializations ON doctors.idspecialization = specializations.id " +
                    "WHERE doctors.id = ?";

    private static final String UPDATE_DOCTOR_BY_ID =
            "UPDATE doctors SET (firstname, secondname, idspecialization, phone, description) " +
                    "= (?, ?, ?, ?, ?) " +
                    "WHERE id = ?";

    private static final String INSERT_SPECIALIZATIONS_TO_CLINIC =
            "INSERT INTO clinics_specializations " +
                    "(idclinic, idspecialization) VALUES (?, ?)";

    private static final String GET_SPECIALIZATIONS_BY_CLINIC =
            "SELECT specializations.name FROM specializations " +
                    "LEFT JOIN clinics_specializations " +
                    "ON specializations.id = clinics_specializations.idspecialization " +
                    "WHERE clinics_specializations.idclinic = " +
                    "(SELECT clinics.id FROM clinics WHERE email = ?)";

    private static final String INSERT_SCHEDULE_FOR_CLINIC =
            "INSERT INTO clinics_schedules " +
                    "(idclinic, timeofstart, timeofend) " +
                    "VALUES (?, ?, ?) " +
                    "ON CONFLICT (idclinic) DO " +
                    "UPDATE SET (timeofstart, timeofend) = " +
                    "(?, ?)";

    private final DoctorRowMapper doctorRowMapper;
    private final JdbcTemplate jdbcTemplate;
    private final PatientRowMapper patientRowMapper;

    public ProfileDao(final DoctorRowMapper doctorRowMapper,
                      final JdbcTemplate jdbcTemplate,
                      final PatientRowMapper patientRowMapper) {
        this.doctorRowMapper = doctorRowMapper;
        this.jdbcTemplate = jdbcTemplate;
        this.patientRowMapper = patientRowMapper;
    }

    public Patient getPatientByEmail(final String email) {
        try {
            return jdbcTemplate
                    .queryForObject(GET_PATIENT_BY_EMAIL, patientRowMapper, email);
        } catch (EmptyResultDataAccessException exception) {
            return null;
        }
    }

    public Clinic getClinicByEmail(final String email) {
        try {
            return jdbcTemplate
                    .queryForObject(GET_CLINIC_BY_EMAIL, (rs, rowNum) -> {
                        ClinicSchedule schedule = ClinicSchedule.newBuilder()
                                .setTimeOfStart(rs.getTime("timeofstart").toLocalTime())
                                .setTimeOfEnd(rs.getTime("timeofend").toLocalTime())
                                .build();
                        return Clinic.newBuilder()
                                .setId(rs.getInt("id"))
                                .setName(rs.getString("name"))
                                .setEmail(rs.getString("email"))
                                .setUserId(rs.getInt("user_id"))
                                .setUrl(rs.getString("url"))
                                .setDescription(rs.getString("description"))
                                .setPhone(rs.getString("phone"))
                                .setAddress(getAddressByClinicId(rs.getInt("id")))
                                .setSpecializations(getSpecializationByClinicEmail(email))
                                .setSchedule(schedule)
                                .build();
                    }, email);
        } catch (EmptyResultDataAccessException exception) {
            return null;
        }
    }

    private List<String> getSpecializationByClinicEmail(final String email) {
        return jdbcTemplate.queryForList(GET_SPECIALIZATIONS_BY_CLINIC, String.class, email);
    }

    public void updatePatient(final Patient patient) {
        updateUsersById(patient.getUserId(), patient.getEmail());
        jdbcTemplate.update(UPDATE_PATIENT,
                patient.getFirstName(),
                patient.getSecondName(),
                patient.getPhone(),
                patient.getEmail(),
                patient.getId());
    }

    public void updateClinic(final Clinic clinic) {
        updateUsersById(clinic.getUserId(), clinic.getEmail());
        addSpecializationsToClinic(clinic);
        setClinicAddress(clinic);
        setScheduleForClinic(clinic);
        jdbcTemplate.update(UPDATE_CLINIC,
                clinic.getName(),
                clinic.getEmail(),
                clinic.getPhone(),
                clinic.getUrl(),
                clinic.getDescription(),
                clinic.getId());
    }

    private void updateUsersById(final int userId,
                                 final String username) {
        String sql = "UPDATE users SET (username) = (?) " +
                "WHERE id = ?";
        jdbcTemplate.update(sql, username, userId);
    }

    private void addSpecializationsToClinic(final Clinic clinic) {
        deleteAllSpecializationsByClinic(clinic);
        List<Integer> specializationsId = clinic.getSpecializations()
                .stream()
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        specializationsId.forEach(idSpec ->
                jdbcTemplate.update(INSERT_SPECIALIZATIONS_TO_CLINIC, clinic.getId(), idSpec));
    }

    private void deleteAllSpecializationsByClinic(final Clinic clinic) {
        String query = "DELETE FROM clinics_specializations WHERE idclinic = ?";
        jdbcTemplate.update(query, clinic.getId());
    }

    private void setScheduleForClinic(final Clinic clinic) {
        ClinicSchedule schedule = clinic.getSchedule();
        jdbcTemplate.update(INSERT_SCHEDULE_FOR_CLINIC, clinic.getId(),
                Time.valueOf(schedule.getTimeOfStart()), Time.valueOf(schedule.getTimeOfEnd()),
                Time.valueOf(schedule.getTimeOfStart()), Time.valueOf(schedule.getTimeOfEnd()));
    }

    private Address getAddressByClinicId(final int clinicId) {
        try {
            return jdbcTemplate.queryForObject(GET_ADDRESS_BY_CLINIC_ID, (rs, rowNum) -> Address.newBuilder()
                    .setCity(rs.getString("city"))
                    .setRegion(rs.getString("region"))
                    .setLocation(rs.getString("location"))
                    .build(), clinicId);
        } catch (EmptyResultDataAccessException exception) {
            return null;
        }
    }

    private void setClinicAddress(final Clinic clinic) {
        Address address = clinic.getAddress();
        int clinicId = clinic.getId();
        if (getAddressByClinicId(clinicId) != null) {
            jdbcTemplate.update(UPDATE_ADDRESS,
                    address.getCity(), address.getLocation(),
                    clinic.getId(), clinic.getId());
        } else {
            jdbcTemplate.update(INSERT_ADDRESS,
                    address.getCity(), address.getLocation(),
                    clinic.getId());
        }
    }

    public List<Doctor> getDoctorsByClinicEmail(final String email) {
        try {
            return jdbcTemplate.query(GET_DOCTORS_BY_CLINIC_ID, doctorRowMapper, email);
        } catch (EmptyResultDataAccessException exception) {
            return null;
        }
    }

    public List<String> getAllRegions() {
        return jdbcTemplate.queryForList(GET_ALL_REGIONS, String.class);
    }

    public List<String> getCityByRegionName(final String name) {
        return jdbcTemplate.queryForList(GET_CITIES_BY_REGION, String.class, name);
    }

    public void insertNewDoctorByClinicEmail(final String clinicEmail,
                                             final Doctor doctor) {
        jdbcTemplate.update(INSERT_NEW_DOCTOR, doctor.getFirstName(),
                doctor.getSecondName(), doctor.getPhone(),
                doctor.getDescription(), clinicEmail,
                Integer.parseInt(doctor.getSpecialization()));
    }

    public List<SpecializationDto> getAllSpecializations() {
        String query = "SELECT id, name FROM specializations ORDER BY name";
        return jdbcTemplate.query(query, (rs, rowNum) -> SpecializationDto.newBuilder()
                .setId(rs.getInt("id"))
                .setName(rs.getString("name"))
                .build());
    }

    public Doctor getDoctorById(final int id) {
        return jdbcTemplate.queryForObject(GET_DOCTOR_BY_ID, doctorRowMapper, id);
    }

    public void updateDoctorById(final Doctor doctor) {
        jdbcTemplate.update(UPDATE_DOCTOR_BY_ID, doctor.getFirstName(),
                doctor.getSecondName(), Integer.parseInt(doctor.getSpecialization()),
                doctor.getPhone(), doctor.getDescription(), doctor.getId());
    }

    public void deleteDoctorById(final int id) {
        String deleteDoctor = "UPDATE doctors SET (status) = " +
                "('deleted') WHERE doctors.id = ?";
        jdbcTemplate.update(deleteDoctor, id);
    }

    public void updateDoctorsSchedule(final int doctorId,
                                      final List<DoctorSchedule> doctorSchedules) {
        String sql = "INSERT INTO doctors_schedules (iddoctor, timeofstart, timeofend, dayofweek) " +
                "VALUES (?, ?, ?, CAST(? AS DAY))";
        deleteAllSchedulesByDoctorId(doctorId);
        doctorSchedules.forEach(ds -> {
            jdbcTemplate.update(sql, doctorId, Time.valueOf(ds.getTimeOfStart()),
                    Time.valueOf(ds.getTimeOfEnd()), ds.getDayOfWeek().name().toLowerCase());
        });
    }

    private void deleteAllSchedulesByDoctorId(final int id) {
        String sql = "DELETE FROM doctors_schedules WHERE iddoctor = ?";
        jdbcTemplate.update(sql, id);
    }

    public List<Service> getServicesByClinic(final String clinicEmail) {
        String sql = "SELECT idclinic, services.name, price, id FROM services_prices " +
                "LEFT JOIN services ON services_prices.idservice = services.id " +
                "WHERE idclinic = (SELECT id FROM clinics WHERE email = ?) ORDER BY services.name";
        return jdbcTemplate.query(sql, (rs, rowNum) -> Service.newBuilder()
                .setId(rs.getInt("id"))
                .setName(rs.getString("name"))
                .setPrice(rs.getBigDecimal("price"))
                .build(), clinicEmail);
    }

    public List<Service> getAllServices(final String clinicEmail) {
        String sql = "SELECT * FROM services WHERE idspecialization = ?";
        List<Integer> listOfClinicSpecializationsId = getListOfClinicSpecializationsId(clinicEmail);

        List<Service> services = new ArrayList<>();
        for (Integer id : listOfClinicSpecializationsId) {
            List<Service> servicesByOneSpec = jdbcTemplate.query(sql, (rs, rowNum) -> Service.newBuilder()
                    .setId(rs.getInt("id"))
                    .setName(rs.getString("name"))
                    .build(), id);
            services.addAll(servicesByOneSpec);
        }
        return services;
    }

    private List<Integer> getListOfClinicSpecializationsId(final String clinicEmail) {
        String sql = "SELECT idspecialization FROM clinics_specializations " +
                "WHERE idclinic = (SELECT id FROM clinics WHERE email = ?)";
        return jdbcTemplate.queryForList(sql, Integer.class, clinicEmail);
    }

    public void addServiceToClinic(final Service service,
                                   final String clinicEmail) {
        String sql = "INSERT INTO services_prices (idclinic, idservice, price)" +
                " VALUES ((SELECT id FROM clinics WHERE email = ?), ?, ?)";
        jdbcTemplate.update(sql, clinicEmail, Integer.parseInt(service.getName()),
                service.getPrice());
    }

    public Optional<Service> getServiceByClinicId(final String clinicEmail,
                                                  final Service serviceFromResp) {
        String sql = "SELECT * FROM services_prices " +
                "WHERE idclinic = (SELECT id FROM clinics WHERE email = ?) " +
                "AND idservice = ?";
        try {
            return jdbcTemplate.queryForObject(sql, (rs, rowNum) -> {
                Service service = Service.newBuilder()
                                .setId(rs.getInt("idservice"))
                                .build();
                return Optional.of(service);
            }, clinicEmail, Integer.parseInt(serviceFromResp.getName()));
        } catch (EmptyResultDataAccessException exception) {
            return Optional.empty();
        }
    }

    public void deleteService(final int id, final String clinicEmail) {
        String sql = "DELETE FROM services_prices WHERE idclinic " +
                "= (SELECT id FROM clinics WHERE email = ?) AND " +
                "idservice = ?";
        jdbcTemplate.update(sql, clinicEmail, id);
    }
}
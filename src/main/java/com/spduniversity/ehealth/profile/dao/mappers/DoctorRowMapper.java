package com.spduniversity.ehealth.profile.dao.mappers;

import com.spduniversity.ehealth.model.Doctor;
import com.spduniversity.ehealth.model.DoctorSchedule;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.DayOfWeek;
import java.util.List;

@Component
public class DoctorRowMapper implements RowMapper<Doctor> {

    private static final String SCHEDULES_BY_DOCTORS_ID_SQL = "SELECT" +
            " doctors.id," +
            " doctors_schedules.dayofweek," +
            " doctors_schedules.timeofstart," +
            " doctors_schedules.timeofend" +
            " FROM doctors" +
            " LEFT JOIN doctors_schedules ON doctors.id = doctors_schedules.iddoctor" +
            " WHERE id = ?";

    private final JdbcTemplate jdbcTemplate;

    public DoctorRowMapper(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Doctor mapRow(ResultSet rs, int rowNum) throws SQLException {
        return Doctor.newBuilder()
                .setId(rs.getInt("id"))
                .setFirstName(rs.getString("firstname"))
                .setSecondName(rs.getString("secondname"))
                .setSpecialization(rs.getString("specialization"))
                .setPhone(rs.getString("phone"))
                .setDescription(rs.getString("description"))
                .setSchedules(getDoctorSchedulesByDoctorId(rs.getInt("id")))
                .build();
    }

    private List<DoctorSchedule> getDoctorSchedulesByDoctorId(final int doctorId) {
        return jdbcTemplate.query(SCHEDULES_BY_DOCTORS_ID_SQL,
                (rs, rowNum) -> {
                    DayOfWeek dayOfWeek = parseDayOfWeek(rs.getString("dayofweek"));
                    if (dayOfWeek != null) {
                        return DoctorSchedule.newBuilder()
                                .setDayOfWeek(dayOfWeek)
                                .setTimeOfStart(rs.getTime("timeofstart").toLocalTime())
                                .setTimeOfEnd(rs.getTime("timeofend").toLocalTime())
                                .build();
                    }
                    return null;
                }, doctorId);
    }

    private DayOfWeek parseDayOfWeek(final String dayOfWeek) {
        for (DayOfWeek day : DayOfWeek.values()) {
            if (day.name().equalsIgnoreCase(dayOfWeek)) {
                return day;
            }
        }
        return null;
    }
}

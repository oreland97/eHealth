package com.spduniversity.ehealth.profile.services;

import com.spduniversity.ehealth.model.Patient;
import com.spduniversity.ehealth.profile.dao.ProfileDao;
import org.springframework.stereotype.Service;

@Service
public class PatientProfileService {

    private final ProfileDao profileDao;

    public PatientProfileService(final ProfileDao profileDao) {
        this.profileDao = profileDao;
    }

    public Patient getPatientByEmail(final String email) {
        return profileDao.getPatientByEmail(email);
    }

    public void setPatientToDao(final Patient patient) {
        profileDao.updatePatient(patient);
    }
}

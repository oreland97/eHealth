package com.spduniversity.ehealth.profile.services;

import com.spduniversity.ehealth.model.Clinic;
import com.spduniversity.ehealth.model.Doctor;
import com.spduniversity.ehealth.model.DoctorSchedule;
import com.spduniversity.ehealth.profile.dao.ProfileDao;
import com.spduniversity.ehealth.profile.dto.DoctorDto;
import com.spduniversity.ehealth.profile.dto.SpecializationDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClinicProfileService {

    private final ProfileDao profileDao;

    public ClinicProfileService(final ProfileDao profileDao) {
        this.profileDao = profileDao;
    }

    public Clinic getClinicByEmail(final String email) {
        return profileDao.getClinicByEmail(email);
    }

    public void setClinicToDao(final Clinic clinic) {
        profileDao.updateClinic(clinic);
    }

    public List<Doctor> getDoctorsByClinicEmail(final String email) {
        return profileDao.getDoctorsByClinicEmail(email);
    }

    public void addNewDoctorToClinic(final String clinicEmail,
                                     final Doctor doctor) {
        profileDao.insertNewDoctorByClinicEmail(clinicEmail, doctor);
    }

    public Doctor getDoctorById(final int id) {
        return profileDao.getDoctorById(id);
    }

    public List<SpecializationDto> getAllSpecializations() {
        return profileDao.getAllSpecializations();
    }

    public void updateDoctorById(final Doctor doctor) {
        profileDao.updateDoctorById(doctor);
    }

    public void updateDoctorSchedule(final DoctorDto doctorDto) {
        List<DoctorSchedule> schedules = doctorDto.getSchedules();
        List<DoctorSchedule> schedulesWithoutNull = schedules
                .stream()
                .filter(doctorSchedule -> doctorSchedule.getTimeOfStart() != null)
                .collect(Collectors.toList());
        profileDao.updateDoctorsSchedule(doctorDto.getId(), schedulesWithoutNull);
    }

    public void deleteDoctorById(final int id) {
        profileDao.deleteDoctorById(id);
    }

    public List<com.spduniversity.ehealth.model.Service> getServicesByClinic(final String clinicEmail) {
        return profileDao.getServicesByClinic(clinicEmail);
    }

    public List<com.spduniversity.ehealth.model.Service> getAllServices(final String clinicEmail) {
        return profileDao.getAllServices(clinicEmail);
    }

    public void addServiceToClinic(final com.spduniversity.ehealth.model.Service service,
                                   final String clinicEmail) {
        profileDao.addServiceToClinic(service, clinicEmail);
    }

    public void deleteService(final int id,
                              final String clinicEmail) {
        profileDao.deleteService(id, clinicEmail);
    }

    public Optional<com.spduniversity.ehealth.model.Service> getServiceByClinicId(final String clinicEmail,
                                                                                  final com.spduniversity.ehealth.model.Service service) {
        return profileDao.getServiceByClinicId(clinicEmail, service);
    }
}

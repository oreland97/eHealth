package com.spduniversity.ehealth.profile.services;

import com.spduniversity.ehealth.profile.dao.ProfileDao;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocalizationService {

    private final ProfileDao profileDao;

    public LocalizationService(final ProfileDao profileDao) {
        this.profileDao = profileDao;
    }

    public List<String> getListOfRegions() {
        return profileDao.getAllRegions();
    }

    public List<String> getListOfCitiesByRegion(final String regionName) {
        return profileDao.getCityByRegionName(regionName);
    }
}

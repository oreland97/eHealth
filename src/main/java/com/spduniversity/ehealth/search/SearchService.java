package com.spduniversity.ehealth.search;

import com.spduniversity.ehealth.model.Clinic;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SearchService {

    private final SearchDao searchDao;

    public SearchService(final SearchDao searchDao) {
        this.searchDao = searchDao;
    }

    public List<Clinic> getClinics(final Map<String, String> params) {
        return searchDao.getClinics(getConditionsForBaseQuery(params));
    }

    private String getConditionsForBaseQuery(final Map<String, String> params) {
        StringBuilder requestParams = new StringBuilder();
        List<String> conditions = new ArrayList<>();
        for (Map.Entry<String, String> pairs : params.entrySet()) {
            if (pairs.getValue() != null)
                conditions.add(String.format("%s.name = '%s'", StringEscapeUtils.escapeSql(pairs.getKey()),
                        StringEscapeUtils.escapeSql(pairs.getValue())));
        }
        if (!conditions.isEmpty()) {
            requestParams.append(conditions.stream().collect(
                    Collectors.joining(" AND ", " WHERE ", "")));
        }
        return requestParams.toString();
    }
}

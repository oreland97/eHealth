package com.spduniversity.ehealth.search;

import com.spduniversity.ehealth.model.Clinic;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/clinics")
public class SearchRestController {

    private final SearchService searchService;
    private final SearchDao searchDao;

    public SearchRestController(final SearchService searchService,
                                final SearchDao searchDao) {
        this.searchService = searchService;
        this.searchDao = searchDao;
    }

    @PostMapping("/search")
    public List<Clinic> getClinics(@RequestBody final Map<String, String> params) {
        return searchService.getClinics(params);
    }

    @GetMapping("/specializations")
    public List<String> getSpecializations() {
        return searchDao.getSpecializations();
    }

    @GetMapping("/regions")
    public List<String> getRegionsList() {
        return searchDao.getRegions();
    }

    @GetMapping("/cities")
    public List<String> getCitiesList(@RequestParam(required = false) final String selectedRegion) {
        return searchDao.getCitiesByRegions(selectedRegion);
    }
}

package com.spduniversity.ehealth.search;

import com.spduniversity.ehealth.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class SearchDao {

    private static final String BASE_QUERY = "SELECT clinics.id, (array_agg(clinics.name))[1] AS name, " +
            "(array_agg(cities.name))[1] AS city, (array_agg(regions.name))[1] AS region, " +
            "(array_agg(addresses.location))[1] AS location, " +
            "string_agg(specializations.name, ', ') AS specialization, (array_agg(clinics.description))[1] AS description " +
            "FROM addresses " +
            "INNER JOIN clinics ON addresses.idclinic = clinics.id " +
            "INNER JOIN cities ON addresses.idcity = cities.id " +
            "INNER JOIN regions ON cities.idregion = regions.id " +
            "INNER JOIN clinics_specializations ON clinics.id = clinics_specializations.idclinic " +
            "INNER JOIN specializations ON clinics_specializations.idspecialization = specializations.id";

    private static final String REGIONS_SQL = "SELECT regions.name FROM regions ORDER BY regions.name";
    private static final String SPECIALIZATIONS_SQL = "SELECT specializations.name FROM specializations ORDER BY specializations.name";
    private static final String CITIES_SQL = "SELECT " +
            "  cities.name " +
            "FROM cities " +
            "  INNER JOIN regions " +
            "    ON cities.idregion = regions.id " +
            " WHERE regions.name = ? " +
            "ORDER BY cities.name";

    private final JdbcTemplate jdbcTemplate;

    public SearchDao(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<String> getRegions() {
        return jdbcTemplate.query(REGIONS_SQL,
                (rs, rowNum) -> rs.getString("name"));
    }

    public List<String> getSpecializations() {
        return jdbcTemplate.query(SPECIALIZATIONS_SQL,
                (rs, rowNum) -> rs.getString("name"));
    }

    public List<String> getCitiesByRegions(final String city) {
        return jdbcTemplate.query(CITIES_SQL,
                (rs, rowNum) -> rs.getString("name"), city);
    }

    public List<Clinic> getClinics(final String query) {
        String groupByCondition = " GROUP BY clinics.id";
        return jdbcTemplate.query(BASE_QUERY + query + groupByCondition,
                (rs, rowNum) -> {
                    Address address = Address.newBuilder()
                            .setRegion(rs.getString("region"))
                            .setCity(rs.getString("city"))
                            .setLocation(rs.getString("location"))
                            .build();
                    return Clinic.newBuilder()
                            .setId(rs.getInt("id"))
                            .setName(rs.getString("name"))
                            .setSpecializations(Arrays.asList(rs.getString("specialization")))
                            .setAddress(address)
                            .setDescription(rs.getString("description"))
                            .build();
                });
    }
}
package com.spduniversity.ehealth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

import static com.spduniversity.ehealth.Role.CLINIC;
import static com.spduniversity.ehealth.Role.PATIENT;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserService userService;
    private final DataSource dataSource;

    public SecurityConfig(final UserService userService,
                          @Qualifier("dataSource") final DataSource dataSource) {
        this.userService = userService;
        this.dataSource = dataSource;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/ajax-files/*", "/bootstrap/css/**", "/bootstrap/js/**",
                        "/img/*", "/jquery/*", "/style/*", "/clinic-profile/**").permitAll()
                .antMatchers("/search", "/api/clinics/*",
                        "/api/login", "/api/clinic/sign_up", "/api/patient/sign_up",
                        "/clinic-sign-up", "/patient-sign-up", "/login", "/api/clinic-info", "/clinic-info", "/").permitAll()
                .antMatchers("/patient-edit", "api/patient/profile",
                        "/booking", "/api/booking/*").hasAuthority(PATIENT.name())
                .antMatchers("/clinic-edit", "/doctors-table",
                        "api/clinic/profile/*", "/api/localization/*", "/services").hasAuthority(CLINIC.name())
                .antMatchers("/records", "api/records/*").authenticated()
                .anyRequest().authenticated()
                .and()
                .formLogin().loginPage("/login").defaultSuccessUrl("/search").permitAll()
                .and()
                .logout().logoutSuccessUrl("/search").permitAll()
                .and()
                .rememberMe()
                .rememberMeCookieName("remember-me-cookie-name")
                .tokenRepository(tokenRepository())
                .tokenValiditySeconds(24 * 60 * 60);
    }

    @Bean
    public PersistentTokenRepository tokenRepository() {
        JdbcTokenRepositoryImpl jdbcTokenRepositoryImpl = new JdbcTokenRepositoryImpl();
        jdbcTokenRepositoryImpl.setDataSource(dataSource);
        return jdbcTokenRepositoryImpl;
    }
}

package com.spduniversity.ehealth.records;

import java.time.LocalDateTime;

public class FeedbackDto {

    private LocalDateTime dateTime;
    private int rating;
    private String description;
    private int idRecord;

    private FeedbackDto() {
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public int getRating() {
        return rating;
    }

    public String getDescription() {
        return description;
    }

    public int getIdRecord() {
        return idRecord;
    }

    public static Builder newBuilder() {
        return new FeedbackDto()
                .new Builder();
    }

    public class Builder {

        private Builder() {
        }

        public Builder setDateTime(final LocalDateTime dateTime) {
            FeedbackDto.this.dateTime = dateTime;
            return this;
        }

        public Builder setRating(final int rating) {
            FeedbackDto.this.rating = rating;
            return this;
        }

        public Builder setDescription(final String description) {
            FeedbackDto.this.description = description;
            return this;
        }

        public Builder setIdRecord(final int idRecord) {
            FeedbackDto.this.idRecord = idRecord;
            return this;
        }

        public FeedbackDto build() {
            return FeedbackDto.this;
        }
    }
}


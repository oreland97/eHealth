package com.spduniversity.ehealth.records;

import com.spduniversity.ehealth.Role;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RecordController {

    @RequestMapping("/records")
    public String getSearch(final Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Role role = (Role) authentication.getAuthorities().stream().findFirst().orElse(null);
        model.addAttribute("role", role);
        return "records";
    }
}

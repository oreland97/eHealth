package com.spduniversity.ehealth.records;

public class StatusDto {

    private int recordId;
    private String status;

    private StatusDto() {
    }

    public int getRecordId() {
        return recordId;
    }

    public String getStatus() {
        return status;
    }

    public Builder newBuilder() {
        return new StatusDto()
                .new Builder();
    }

    public class Builder {

        private Builder() {
        }

        public Builder setRecordId(final int recordId) {
            StatusDto.this.recordId = recordId;
            return this;
        }

        public Builder setStatus(final String status) {
            StatusDto.this.status = status;
            return this;
        }

        public StatusDto build() {
            return StatusDto.this;
        }

    }
}

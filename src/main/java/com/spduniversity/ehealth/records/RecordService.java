package com.spduniversity.ehealth.records;

import com.spduniversity.ehealth.booking.Record;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecordService {

    private final RecordDao recordDao;

    public RecordService(final RecordDao recordDao) {
        this.recordDao = recordDao;
    }

    public List<Record> getRecordsByEmail(final String role, final String email) {
        return recordDao.getRecords(role, email);
    }

    public void addFeedbackToRecord(final FeedbackDto feedback) {
        recordDao.insertFeedback(feedback);
    }

    public void changeRecordStatus(final StatusDto statusDto) {
        recordDao.updateRecordStatus(statusDto.getRecordId(), statusDto.getStatus());
    }
}

package com.spduniversity.ehealth.records;

import com.spduniversity.ehealth.booking.Record;
import com.spduniversity.ehealth.booking.Status;
import com.spduniversity.ehealth.model.Doctor;
import com.spduniversity.ehealth.model.Patient;
import com.spduniversity.ehealth.model.Service;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import static com.spduniversity.ehealth.Role.CLINIC;
import static com.spduniversity.ehealth.Role.PATIENT;

@Repository
public class RecordDao {

    private static final String RECORDS_BY_CLINIC_EMAIL = "SELECT" +
            " records.id," +
            " records.date," +
            " records.time," +
            " records.idpatient," +
            " records.iddoctor," +
            " records.idservice," +
            " records.status," +
            " feedbacks.id AS idFeedback" +
            " FROM records" +
            " INNER JOIN doctors ON records.iddoctor = doctors.id" +
            " INNER JOIN clinics ON doctors.idclinic = clinics.id" +
            " INNER JOIN services ON records.idservice = services.id" +
            " LEFT JOIN feedbacks ON feedbacks.idrecord = records.id " +
            " WHERE clinics.email = ?";

    private static final String RECORDS_BY_PATIENT_EMAIL = "SELECT" +
            " records.id," +
            " records.date," +
            " records.time," +
            " records.iddoctor," +
            " records.idservice," +
            " records.status," +
            " feedbacks.id AS idFeedback" +
            " FROM records" +
            " INNER JOIN doctors ON records.iddoctor = doctors.id" +
            " INNER JOIN patients ON records.idpatient = patients.id" +
            " INNER JOIN services ON records.idservice = services.id" +
            " LEFT JOIN feedbacks ON feedbacks.idrecord = records.id " +
            " WHERE patients.email = ?";

    private final JdbcTemplate jdbcTemplate;

    public RecordDao(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Record> getRecords(final String role, final String email) {
        String query = "";
        if (role.equals(CLINIC.name())) {
            query = RECORDS_BY_CLINIC_EMAIL;
        } else if (role.equals(PATIENT.name())) {
            query = RECORDS_BY_PATIENT_EMAIL;
        }
        return jdbcTemplate.query(query, (rs, rowNum) -> {
            Record record = new Record();
            record.setId(rs.getInt("id"));
            record.setDate(LocalDate.parse(rs.getString("date")));
            record.setTime(LocalTime.parse(rs.getString("time")));
            if (role.equals(CLINIC.name())) {
                record.setPatient(getPatientById(rs.getInt("idpatient")));
            }
            record.setDoctor(getDoctorByID(rs.getInt("iddoctor")));
            record.setService(getServiceById(rs.getInt("idservice")));
            record.setStatus(Status.valueOf(rs.getString("status").toUpperCase()));
            Integer feedbackId = rs.getInt("idFeedback");
            if (feedbackId != 0) {
                record.setFeedback(getFeedbackById(rs.getInt("idFeedback")));
            }
            return record;
        }, email);
    }

    private Patient getPatientById(final int idPatient) {
        String getPatientQuery = "SELECT " +
                " patients.id," +
                " patients.firstname," +
                " patients.secondname" +
                " FROM patients WHERE id = ?";
        return jdbcTemplate.queryForObject(getPatientQuery,
                (rs, rowNum) -> Patient.newBuilder()
                        .setId(rs.getInt("id"))
                        .setFirstName(rs.getString("firstname"))
                        .setSecondName(rs.getString("secondname"))
                        .build(), idPatient);
    }

    private Doctor getDoctorByID(final int idDoctor) {
        String getPatientQuery = "SELECT " +
                " doctors.id," +
                " doctors.firstname," +
                " doctors.secondname" +
                " FROM doctors WHERE id = ?";
        return jdbcTemplate.queryForObject(getPatientQuery,
                (rs, rowNum) -> Doctor.newBuilder()
                        .setId(rs.getInt("id"))
                        .setFirstName(rs.getString("firstname"))
                        .setSecondName(rs.getString("secondname"))
                        .build(), idDoctor);
    }

    private Service getServiceById(final int idService) {
        String getServiceQuery = "SELECT " +
                " services.id," +
                " services.name" +
                " FROM services WHERE id = ?";
        return jdbcTemplate.queryForObject(getServiceQuery,
                (rs, rowNum) -> Service.newBuilder()
                        .setId(rs.getInt("id"))
                        .setName(rs.getString("name"))
                        .build(), idService);

    }

    private Feedback getFeedbackById(final int idFeedback) {
        String getFeedbackQuery = "SELECT " +
                " feedbacks.id," +
                " feedbacks.rating," +
                " feedbacks.description," +
                " feedbacks.datetime" +
                " FROM feedbacks WHERE id = ?";
        return jdbcTemplate.queryForObject(getFeedbackQuery,
                (rs, rowNum) -> {
                    Feedback feedback = new Feedback();
                    feedback.setId(rs.getInt("id"));
                    feedback.setRating(rs.getInt("rating"));
                    feedback.setDescription(rs.getString("description"));
                    feedback.setDateTime(rs.getTimestamp("datetime").toLocalDateTime());
                    return feedback;
                }, idFeedback);

    }

    public void insertFeedback(final FeedbackDto feedback) {
        String insertQuery = "INSERT INTO feedbacks " +
                "(rating, description, idrecord, datetime) " +
                "VALUES (?, ?, ?, ?) ";
        jdbcTemplate.update(insertQuery, feedback.getRating(),
                feedback.getDescription(), feedback.getIdRecord(),
                Timestamp.valueOf(LocalDateTime.now()));
    }

    public void updateRecordStatus(final int recordId, final String status) {
        String updateQuery = "UPDATE records " +
                "SET status = CAST(? AS STATUS) " +
                "WHERE records.id = ?";
        jdbcTemplate.update(updateQuery, status, recordId);
    }
}







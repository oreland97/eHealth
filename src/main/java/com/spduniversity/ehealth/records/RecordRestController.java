package com.spduniversity.ehealth.records;

import com.spduniversity.ehealth.Role;
import com.spduniversity.ehealth.booking.Record;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/records")
public class RecordRestController {

    private final RecordService recordService;

    public RecordRestController(RecordService recordService) {
        this.recordService = recordService;
    }

    @GetMapping
    public ResponseEntity getRecordsByEmail() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Role role = (Role) authentication.getAuthorities().stream().findFirst().orElse(null);
        String email = authentication.getName();
        List<Record> records = new ArrayList<>();
        if (role != null) {
            records = recordService.getRecordsByEmail(role.name(), email);
        }
        return ResponseEntity.ok(records);

    }

    @PostMapping("/add-feedback")
    public ResponseEntity addFeedbackForRecord(@RequestBody final FeedbackDto feedback) {
        recordService.addFeedbackToRecord(feedback);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/update-status")
    public ResponseEntity updateRecordStatus(@RequestBody final StatusDto statusDto) {
        recordService.changeRecordStatus(statusDto);
        return ResponseEntity.ok().build();
    }
}
